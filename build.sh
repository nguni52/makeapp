#!/bin/sh
mvn clean package -DskipTests
cd docker
docker-compose up -d
#nohup java -jar target/makeapp.jar &
docker logs --follow $(docker ps -q -f name=docker_makeapp_1)