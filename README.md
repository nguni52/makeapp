# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

* Quick summary
* Version
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

### How do I get set up? ###

* Summary of set up
* Configuration
* Dependencies
* Database configuration
* How to run tests


To test the links from the terminal, run the following.
```
#!java

curl -i -X POST -H "Content-Type:application/json" "http://localhost:9000/api/user/register" 
-d '{"username":"phakelam", "firstName":"Matiisetso", "lastName":"Phakela", "password":"tiisetso",
 "email":"phakelam@icloud.com"}'

curl -i -X POST -H "Content-Type:application/json" "http://localhost:9000/api/actor/create" 
-d '{"firstName":"Lenka", "lastName":"Lenkoe", "gender": "male", "number":"+27-78-029-0917"}'
```

https://currencylayer.com/quickstart

* Deployment instructions
We are assuming you are using ubuntu

###Install apache2 and enable proxy modules ###
```
    sudo apt-get install apache2
    
    sudo a2enmod proxy
    sudo a2enmod proxy_ajp
    sudo a2enmod proxy_http
    
```    
### add this following to VirtualHost tag ###

```
    sudo vim /etc/apache2/sites-enabled/000-default.conf
    ProxyPreserveHost On
    <Proxy *>
       AddDefaultCharset Off
       Order deny,allow
       Allow from all
    </Proxy>
    ProxyPass / http://0.0.0.0:9000/
    ProxyPassReverse / http://0.0.0.0:9000/
```    
### Save and restart apache2 ###

```    
    sudo service apache2 restart
```

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Nguni Phakela (nguni52@gmail.com)
  Vovas Phillips (mkmaposa@gmail.com)
* Other community or team contact