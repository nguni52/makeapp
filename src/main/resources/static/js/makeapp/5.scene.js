/**
 * Created by mkmaposa on 2016/06/02.
 */

var scene = new Scene();
var SCENE_API_BASE_URL = $("#scene_api_base_url").val();
var SCENE_BASE_URL = $("#scene_base_url").val();
var $alert = $('.alert');
var $table = $("#scene_list_table");

function Scene() {
    this.$sceneModal = $('#add_scene_modal');
    this.addNewScene = function () {
        this.$sceneModal.modal('show');
    };
    this.save = function () {
        var scene = {};

        scene.number = $("#number").val();
        scene.description = $("#description").val();
        scene.intOrExt = $("#intOrExt").val();
        scene.dayOrNight = $("#dayOrNight").val();
        scene.description = $("#description").val();
        scene.screenDay = $("#screenDay").val();
        scene.shootingDate = $("#shootingDate").val();
        scene.episodeId = $("#episodeId").val();

        $.ajax({
            type: this.$sceneModal.data('id') ? 'put' : 'post',
            url: SCENE_API_BASE_URL + 'add/' + (this.$sceneModal.data('id') || ''),
            data: JSON.stringify(scene),
            contentType: 'application/json',
            dataType: 'json',
            success: function (response) {
                if (response.status) {
                    refreshTable();
                }
                displayAlert(response);
            }
        });
    };
    this.showAlert = function (title, type) {
        $alert.attr('class', 'alert alert-' + type || 'success')
            .html('<i class="glyphicon glyphicon-check"></i> ' + title).show();
        setTimeout(function () {
            $alert.hide();
        }, 5000);
    };
    this.showModal = function (title, row) {
        this.$sceneModal.data('id', row.id);
        this.$sceneModal.find('.modal-title').text(title);
        for (var name in row) {
            this.$sceneModal.find('input[name="' + name + '"]').val(row[name]);
        }
        this.$sceneModal.modal('show');
    };
}

$(function () {
    $table.bootstrapTable({
        url: SCENE_API_BASE_URL + $("#episodeId").val() + '/list',
        search: true,
        pagination: true,
        sortOrder: 'asc',
        pageSize: 25
    });

    $(document).on('click', '#scene_add', function () {
        scene.addNewScene();
    });

    var $sceneForm = $("#scene_form");
    $sceneForm.validate({
        rulea: {
            name: "required",
            number: "required"
        },
        messages: {
            name: "Please specify the episode name",
            number: "Please specify the episode number"
        }
    });
    $sceneForm.on('submit', function (e) {
        e.preventDefault();
        if ($(this).valid()) {
            scene.save();
        }
    });
});


function displayAlert(response) {
    scene.$sceneModal.modal('hide');
    scene.showAlert(response.message, 'success');
}

function refreshTable() {
    $table.bootstrapTable('refresh');
}

// update and delete events
window.actionEvents = {
    'click .view': function (e, value, row) {
        var sceneId = row["id"];
        window.location.href = SCENE_BASE_URL + 'details/' + sceneId;
    },
    'click .update': function (e, value, row) {
        scene.showModal($(this).attr('title'), row);
    },
    'click .remove': function (e, value, row) {
        if (confirm('Are you sure to delete this item?')) {
            $.ajax({
                url: SCENE_API_BASE_URL + 'delete/' + row.id,
                type: 'delete',
                success: function (response) {
                    refreshTable();
                    scene.showAlert(response.message, 'success');
                },
                error: function (response) {
                    scene.showAlert(response.message, 'danger');
                }
            })
        }
    }
};

function queryParams(params) {
    return {};
}
function actionFormatter(value) {
    return [
        '<a class="view" href="javascript:" title="View Item Details"><i class="glyphicon glyphicon-eye-open"></i></a>',
        '<a class="update" href="javascript:" title="Update Item"><i class="glyphicon glyphicon-edit"></i></a>',
        '<a class="remove" href="javascript:" title="Delete Item"><i class="glyphicon glyphicon-remove-circle"></i></a>',
    ].join('&nbsp;|&nbsp;');
}
