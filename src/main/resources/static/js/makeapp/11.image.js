/**
 * Created by mkmaposa on 2016/06/02.
 */

var image = new Image();
var CHARACTER_API_BASE_URL = $("#character_api_base_url").val();
var CHARACTER_BASE_URL = $("#character_base_url").val();
var CHARACTER_ID = $("#character_id").val();
var SCENE_ID = $("#scene_id").val();
var $alert = $('.alert');
var $table = $("#scene_list_table");

function Image() {
    this.$characterImageModal = $('#upload_character_image_modal');
    this.uploadCharacterImage = function () {
        this.$characterImageModal.modal('show');
    };
    this.save = function () {
        var scene = {};
        scene.sceneId = $("#sceneId").val();

        var fd = new FormData();
        fd.append( 'uploadImage', $( '#uploadImage' )[0].files[0] );
        fd.append('comment', $("#comments").val());

        $.ajax({
            //type: this.$characterImageModal.data('id') ? 'put' : 'post',
            //url: CHARACTER_API_BASE_URL + 'upload/' + (this.$characterImageModal.data('id') || ''),
            url: CHARACTER_API_BASE_URL + 'upload/'+SCENE_ID+'/'+CHARACTER_ID,
            type: 'POST',
            data: fd,
            processData: false,
            contentType: false,
            enctype: 'multipart/form-data',
            dataType: 'multipart/form-data',
            success: function (response) {
                if (response.status) {
                    //refreshTable();
                    reloadPage();
                }
                displayAlert(response);
                reloadPage();
            },
            error:function (response) {
                if (response.status) {
                    console.log(response);
                }
                displayAlert(response);
            }
        });
    };
    this.showAlert = function (title, type) {
        $alert.attr('class', 'alert alert-' + type || 'success')
            .html('<i class="glyphicon glyphicon-check"></i> ' + title).show();
        setTimeout(function () {
            $alert.hide();
        }, 5000);
    };
    this.showModal = function (title, row) {
        this.$characterImageModal.data('id', row.id);
        console.log('MY ID IS ',this.$characterImageModal.data('id', row.id));
        this.$characterImageModal.find('.modal-title').text(title);
        for (var name in row) {
            this.$characterImageModal.find('input[name="' + name + '"]').val(row[name]);
        }
        this.$characterImageModal.modal('show');
    };
}

$(function () {
    $table.bootstrapTable({
        url: CHARACTER_API_BASE_URL + $("#episodeId").val() + '/list',
        search: true,
        pagination: true,
        sortOrder: 'asc',
        pageSize: 25
    });

    $(document).on('click', '#character_image_upload', function () {
        image.uploadCharacterImage();
    });

    var $characterImageUploadForm = $("#character_image_upload_form");
    $characterImageUploadForm.validate({
        rulea: {
            name: "required",
            number: "required"
        },
        messages: {
            name: "Please specify the episode name",
            number: "Please specify the episode number"
        }
    });
    $characterImageUploadForm.on('submit', function (e) {
        e.preventDefault();
        if ($(this).valid()) {
            image.save();
        }
    });
});


function displayAlert(response) {
    image.$characterImageModal.modal('hide');
    image.showAlert(response.message, 'success');
}

function refreshTable() {
    $table.bootstrapTable('refresh');
}

function reloadPage(){
    location.reload();
}

// update and delete events
window.actionEvents = {
    'click .view': function (e, value, row) {
        var sceneId = row["id"];
        window.location.href = CHARACTER_BASE_URL + 'details/' + sceneId;
    },
    'click .update': function (e, value, row) {
        scene.showModal($(this).attr('title'), row);
    },
    'click .remove': function (e, value, row) {
        if (confirm('Are you sure to delete this item?')) {
            $.ajax({
                url: CHARACTER_API_BASE_URL + 'delete/' + row.id,
                type: 'delete',
                success: function (response) {
                    refreshTable();
                    scene.showAlert(response.message, 'success');
                },
                error: function (response) {
                    scene.showAlert(response.message, 'danger');
                }
            })
        }
    }
};

function queryParams(params) {
    return {};
}
function actionFormatter(value) {
    return [
        '<a class="view" href="javascript:" title="View Item Details"><i class="glyphicon glyphicon-eye-open"></i></a>',
        '<a class="update" href="javascript:" title="Update Item"><i class="glyphicon glyphicon-edit"></i></a>',
        '<a class="remove" href="javascript:" title="Delete Item"><i class="glyphicon glyphicon-remove-circle"></i></a>',
    ].join('&nbsp;|&nbsp;');
}
