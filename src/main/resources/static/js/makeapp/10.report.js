/**
 * Created by nguni52 on 2017/06/29.
 */
var REPORT_SCENE_API_BASE_URL = $("#report_scene_base_url").val();

$(function () {
    $table.bootstrapTable({
        url: REPORT_SCENE_API_BASE_URL + '/list',
        search: true,
        pagination: true,
        sortOrder: 'asc',
        pageSize: 25
    });
    $('#hover, #striped, #condensed').click(function () {
        var classes = 'table';
        if ($('#hover').prop('checked')) {
            classes += ' table-hover';
        }
        if ($('#condensed').prop('checked')) {
            classes += ' table-condensed';
        }
        $('#table-style').bootstrapTable('destroy')
            .bootstrapTable({
                classes: classes,
                striped: $('#striped').prop('checked')
            });
    });
});


function queryParams(params) {
    return {};
}
function actionFormatter(value) {
    return [
        '<a class="view" href="javascript:" title="View Scene Details"><i class="glyphicon glyphicon-eye-open"></i></a>'
    ].join('&nbsp;|&nbsp;');
}
// update and delete events
window.actionEvents = {
    'click .view': function (e, value, row) {
        var sceneId = row["id"];
        window.location.href = REPORT_SCENE_API_BASE_URL + '/details/' + sceneId;
    }
};