/**
 * Created by mkmaposa on 2016/05/30.
 */

var character = new Character();

var CHARACTER_API_BASE_URL = $("#character_api_base_url").val();
var CHARACTER_BASE_URL = $("#character_base_url").val();
var SCENE_CHARACTER_BASE_URL = $("#scene_character_base_url").val();
var ACTOR_BASE_URL = $("#actor_base_url").val();
var API_SCENE_CHARACTER = $("#api_scene_character").val();
var API_SCENE_CHARACTER_SAVE = API_SCENE_CHARACTER + '/save';
var $alert = $('.alert');
var $table = $("#character_list_table");
var $sceneCharacterTable = $("#scene_details_character_list_table");

function Character() {
    this.$characterModal = $('#add_character_modal');
    this.$sceneCharacterModal = $("#add_character_checkbox_modal");
    console.log("We are about to show the modal window");
    this.addNewCharacter = function () {
        console.log("We are about to show the modal window");
        this.$characterModal.modal('show');
    };
    this.save = function () {
        var character = {};

        character.id = $("#actorId :selected").val();
        character.characterFirstName = $("#characterFirstName").val();
        character.characterLastName = $("#characterLastName").val();
        character.characterId = $("#characterId").val();

        $.ajax({
            type: this.$characterModal.data('id') ? 'put' : 'post',
            url: CHARACTER_API_BASE_URL + 'add/' + (this.$characterModal.data('id') || ''),
            data: JSON.stringify(character),
            contentType: 'application/json',
            dataType: 'json',
            success: function (response) {
                if (response.status) {
                    refreshTable();
                }
                displayAlert(response);
            }
        });
    };
    this.showAlert = function (title, type) {
        $alert.attr('class', 'alert alert-' + type || 'success')
            .html('<i class="glyphicon glyphicon-check"></i> ' + title).show();
        setTimeout(function () {
            $alert.hide();
        }, 5000);
    };
    this.showModal = function (title, row) {
        this.$characterModal.data('id', row.id);
        this.$characterModal.find('.modal-title').text(title);
        for (var name in row) {
            this.$characterModal.find('input[name="' + name + '"]').val(row[name]);
        }
        this.$characterModal.modal('show');
    };
    this.populateActorDropdowns = function () {
        // get a list of all actors
        $.get(ACTOR_BASE_URL + '/list', function (response) {
            // add them to the dom
            console.log(response);
            $.each(response, function (i, row) {
                console.log(i, row);
                var name = row.firstName + " " + row.lastName;
                var value = row.id;
                var $html = "<option value='" + value + "'>" + name + "</option>";
                $("#actorId").append($html);
            });
        }).success(function (response) {
            refreshTable();
        }).error(function (response) {
            displayAlert(response);
        });

    };
    this.addNewSceneCharacters = function () {
        this.$sceneCharacterModal.modal('show');
    };
    this.saveCharactersViaCheckboxes = function ($form) {
        // get all checked checkboxes
        var $checked = [];
        $(".characterId:checked").each(function () {
            $checked.push(this.value);
        });
        if ($checked.length > 0) {
            $.ajax({
                type: 'POST',
                url: API_SCENE_CHARACTER_SAVE,
                data: "characterIds=" + $checked,
                success: function (response) {
                    if (response.status) {
                        refreshSceneCharacterTable();
                    }
                    displayAlert(response);
                }
            })
        }
    };
}

$(function () {
    $("#character_list_table").bootstrapTable({
        url: CHARACTER_API_BASE_URL + 'list',
        pagination: true,
        sortOrder: 'asc',
        pageSize: 25
    });

    $("#scene_details_character_list_table").bootstrapTable({
        url: API_SCENE_CHARACTER,
        pagination: true,
        sortOrder: 'asc',
        pageSize: 25
    });

    var $scene_page = $("#scene_page");
    if (typeof $scene_page !== 'undefined') {
        character.populateActorDropdowns();
    }

    $('#hover, #striped, #condensed').click(function () {
        var classes = 'table';
        if ($('#hover').prop('checked')) {
            classes += ' table-hover';
        }
        if ($('#condensed').prop('checked')) {
            classes += ' table-condensed';
        }
        $('#table-style').bootstrapTable('destroy')
            .bootstrapTable({
                classes: classes,
                striped: $('#striped').prop('checked')
            });
    });

    $(document).on('click', '#character_add', function () {
        console.log("We have clicked character add link");
        character.addNewCharacter();
    });

    $(document).on('click', '#scene_character_add', function () {
        character.addNewSceneCharacters();
    });

    var $characterForm = $("#character_form");
    $characterForm.validate({
        rules: {
            firstName: "required",
            lastName: "required",
            characterId: "required"
        },
        messages: {
            firstName: "Please enter first name",
            lastName: "Please enter last name",
            characterId: "Please enter character ID number"
        }
    });

    $characterForm.on('submit', function (e) {
        e.preventDefault();

        console.log('saving character form');
        if ($characterForm.valid()) {
            character.save();
            console.log("character form is valid.");
        } else {
            console.log("character Form is not valid");
        }
    });

    var $characterCheckBoxForm = $("#character_checkbox_form");
    $characterCheckBoxForm.on('submit', function (e) {
        console.log("We are submitting the checkboxes");
        e.preventDefault();

        character.saveCharactersViaCheckboxes($(this));
    })
});


function queryParams(params) {
    return {};
}
function actionFormatter(value) {
    return [
        '<a class="view" href="javascript:" title="View Item Details"><i class="glyphicon glyphicon-eye-open"></i></a>',
        '<a class="update" href="javascript:" title="Update Item"><i class="glyphicon glyphicon-edit"></i></a>',
        '<a class="remove" href="javascript:" title="Delete Item"><i class="glyphicon glyphicon-remove-circle"></i></a>',
    ].join('&nbsp;|&nbsp;');
}

function characterActionFormatter(value) {
    return [
        '<a class="view" href="javascript:" title="View Item Details"><i class="glyphicon glyphicon-eye-open"></i></a>',
        '<a class="remove" href="javascript:" title="Delete Item"><i class="glyphicon glyphicon-remove-circle"></i></a>',
    ].join('&nbsp;|&nbsp;');
}

// update and delete events
window.actionEvents = {
    'click .view': function (e, value, row) {
        var characterId = row["id"];
        window.location.href =  SCENE_CHARACTER_BASE_URL+ '/' + characterId;
    },
    'click .update': function (e, value, row) {
        character.showModal($(this).attr('title'), row);
    },
    'click .remove': function (e, value, row) {
        if (confirm('Are you sure to delete this item?')) {
            $.ajax({
                url: CHARACTER_API_BASE_URL + 'delete/' + row.id,
                type: 'delete',
                success: function (response) {
                    refreshTable();
                    character.showAlert(response.message, 'success');
                },
                error: function (response) {
                    character.showAlert(response.message, 'danger');
                }
            })
        }
    }
};

window.characterActionEvents = {
    'click .view': function (e, value, row) {
        var characterId = row["id"];
        window.location.href =  SCENE_CHARACTER_BASE_URL+ '/' + characterId;
    },
    'click .remove': function (e, value, row) {
        if (confirm('Are you sure to delete this item?')) {
            $.ajax({
                url: API_SCENE_CHARACTER + '/' + row.id,
                type: 'delete',
                success: function (response) {
                    refreshSceneCharacterTable();
                    character.showAlert(response.message, 'success');
                },
                error: function (response) {
                    character.showAlert(response.message, 'danger');
                }
            })
        }
    }
};

function displayAlert(response) {
    character.$characterModal.modal('hide');
    character.showAlert(response.message, 'success');
}

function refreshTable() {
    $table.bootstrapTable('refresh');
}

function refreshSceneCharacterTable() {
    $sceneCharacterTable.bootstrapTable('refresh');
}