/**
 * Created by nguni52 on 2016/06/27.
 */
var USER_BASE_URL = $("#user_base_url").val();
var USER_API_BASE_URL = $("#user_api_base_url").val();

var $table = $("#user_list_table");

var user = new User();
var alert = new Alert();
function User() {
    this.$userModal = $("#add_user_modal");
    this.addNewUser = function () {
        this.$userModal.modal('show');
    };
    this.save = function () {
        var user = {};
        user.username = $("#username").val();
        user.firstName = $("#firstName").val();
        user.lastName = $("#lastName").val();
        user.email = $("#email").val();
        user.password = $("#password").val();

        $.get(USER_API_BASE_URL + 'getCurrentUser', function (data) {
            user.adminId = data;
        }).done(function () {
            $.ajax({
                type: this.$userModal.data('id') ? 'put' : 'post',
                url: USER_API_BASE_URL + 'register/' + (this.$userModal.data('id') || ''),
                data: JSON.stringify(user),
                contentType: 'application/json',
                dataType: 'json',
                success: function (response) {
                    if (response.status) {
                        user.$userModal.modal('hide');
                        refreshTable();
                    }
                    alert.showAlert(response);
                }
            });
        });


    };
    this.showModal = function (title, row) {
        this.$userModal.data('id', row._id);
        this.$userModal.find('.modal-title').text(title);
        for (var name in row) {
            this.$userModal.find('input[name="' + name + '"]').val(row[name]);
        }
        this.$userModal.modal('show');
    };
}

$(function () {
    $table.bootstrapTable({
        url: USER_API_BASE_URL + 'list',
        search: true,
        pagination: true,
        sortOrder: 'asc',
        pageSize: 25
    });
    $('#hover, #striped, #condensed').click(function () {
        var classes = 'table';
        if ($('#hover').prop('checked')) {
            classes += ' table-hover';
        }
        if ($('#condensed').prop('checked')) {
            classes += ' table-condensed';
        }
        $('#table-style').bootstrapTable('destroy')
            .bootstrapTable({
                classes: classes,
                striped: $('#striped').prop('checked')
            });
    });

    $(document).on('click', '#user_add', function () {
        user.addNewUser();
    });

    var $userForm = $("#user_form");
    $userForm.validate({
        rules: {
            username: "required",
            firstName: "required",
            lastName: "required",
            password: "required",
            confirm_password: {
                required: true,
                equalTo: "#password"
            }
        },
        messages: {
            username: "Please enter the username",
            firstName: "Please enter the first name",
            lastName: "Please enter the last name",
            password: "Please enter the password",
            confirm_password: {
                required: "Please enter the confirmation password",
                equalTo: "Password confirmation should equal password"
            }
        }
    });
    $userForm.on('submit', function (e) {
        e.preventDefault();
        if ($(this).valid()) {
            user.save();
        }
    })
});


function queryParams(params) {
    return {};
}
function actionFormatter(value) {
    return [
        '<a class="view" href="javascript:" title="View Item Details"><i class="glyphicon glyphicon-eye-open"></i></a>',
        '<a class="update" href="javascript:" title="Update Item"><i class="glyphicon glyphicon-edit"></i></a>',
        '<a class="remove" href="javascript:" title="Delete Item"><i class="glyphicon glyphicon-remove-circle"></i></a>',
    ].join('&nbsp;|&nbsp;');
}
// update and delete events
window.actionEvents = {
    'click .view': function (e, value, row) {
        var userId = row["id"];
        window.location.href = USER_BASE_URL + '/details/' + userId;
    },
    'click .update': function (e, value, row) {
        user.showModal($(this).attr('title'), row);
    },
    'click .remove': function (e, value, row) {
        if (confirm('Are you sure to delete this item?')) {
            $.ajax({
                url: USER_API_BASE_URL + 'delete/' + row.id,
                type: 'delete',
                success: function (response) {
                    refreshTable();
                    alert.showAlert(response.message, 'success');
                },
                error: function (response) {
                    alert.showAlert(response.message, 'danger');
                }
            })
        }
    }
};

function refreshTable() {
    $table.bootstrapTable('refresh');
}