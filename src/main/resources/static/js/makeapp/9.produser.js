/**
 * Created by nguni52 on 2016/06/01.
 */
var produser = new ProductionEmail();
var PRODUSER_API_BASE_URL = $("#produser_api_base_url").val();
var PRODUSER_BASE_URL = $("#produser_base_url").val();
var $alert = $('.alert');
var $table = $("#produser_list_table");

function ProductionEmail() {
    this.$produserModal = $('#add_produser_modal');
    this.addNewProdUser = function () {
        this.$produserModal.modal('show');
    };
    this.save = function () {
        // @ToDo - vovas
        // same variable here as the first variable in the class. It can cause a lot of conflicts
        // var produser = {}
        var prodUserData = {};
        prodUserData.email = $("#email").val();
        prodUserData.productionId = $("#productionId").val();

        $.ajax({
            type: this.$produserModal.data('id') ? 'put' : 'post',
            url: PRODUSER_API_BASE_URL + 'add/email/' + (this.$produserModal.data('id') || ''),
            data: JSON.stringify(prodUserData),
            contentType: 'application/json',
            dataType: 'json',
            success: function (response) {
                if (response.status) {
                    refreshTable();
                    if (typeof produser.$produserModal.data('id') !== 'undefined') {
                        produser.$produserModal.removeData('id');
                    }

                }
                displayAlert(response);
            }
        });
    };
    this.showAlert = function (title, type) {
        $alert.attr('class', 'alert alert-' + type || 'success')
            .html('<i class="glyphicon glyphicon-check"></i> ' + title).show();
        setTimeout(function () {
            $alert.hide();
        }, 5000);
    };
    this.showModal = function (title, row) {
        this.$produserModal.data('id', row.id);
        this.$produserModal.find('.modal-title').text(title);
        for (var name in row) {
            this.$produserModal.find('input[name="' + name + '"]').val(row[name]);
        }
        this.$produserModal.modal('show');
    };
}

$(function () {
    $table.bootstrapTable({
        url: PRODUSER_API_BASE_URL + $("#productionId").val()+'/email',
        search: true,
        pagination: true,
        sortOrder: 'asc',
        pageSize: 25
    });

    $(document).on('click', '#produser_add', function () {
        produser.addNewProdUser();
    });

    var $produserForm = $("#produser_form");
    $produserForm.validate({
        rules: {
            email: "required",
        },
        messages: {
            email: "Please enter email",
        }
    });
    $produserForm.on('submit', function (e) {
        e.preventDefault();
        if ($(this).valid()) {
            produser.save();
        }
    });
});


function displayAlert(response) {
    produser.$produserModal.modal('hide');
    produser.showAlert(response.message, 'success');
}

function refreshTable() {
    $table.bootstrapTable('refresh');
}

// update and delete events
window.actionEvents = {
    'click .update': function (e, value, row) {
        produser.showModal($(this).attr('title'), row);
    },
    'click .remove': function (e, value, row) {
        if (confirm('Are you sure to delete this item?')) {
            $.ajax({
                url: PRODUSER_API_BASE_URL + 'delete/email/' + row.id,
                type: 'delete',
                success: function (response) {
                    refreshTable();
                    produser.showAlert(response.message, 'success');
                },
                error: function (response) {
                    produser.showAlert(response.message, 'danger');
                }
            })
        }
    }
};

function queryParams(params) {
    return {};
}
function actionFormatter(value) {
    return [
        '<a class="update" href="javascript:" title="Update Item"><i class="glyphicon glyphicon-edit"></i></a>',
        '<a class="remove" href="javascript:" title="Delete Item"><i class="glyphicon glyphicon-remove-circle"></i></a>',
    ].join('&nbsp;|&nbsp;');
}
