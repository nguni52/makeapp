/**
 * Created by nguni52 on 2016/05/25.
 */
!function ($) {
    $(document).on("click","ul.nav li.parent > a > span.icon", function(){
        $(this).find('em:first').toggleClass("glyphicon-minus");
    });
    $(".sidebar span.icon").find('em:first').addClass("glyphicon-plus");
}(window.jQuery);

$(window).on('resize', function () {
    if ($(window).width() > 768) $('#sidebar-collapse').collapse('show')
})
$(window).on('resize', function () {
    if ($(window).width() <= 767) $('#sidebar-collapse').collapse('hide')
});


var alert = new Alert();

function Alert() {
    this.$alert = $('.alert');
    this.showAlert = function (title, type) {
        this.$alert.attr('class', 'alert alert-' + type || 'success')
            .html('<i class="glyphicon glyphicon-check"></i> ' + title).show();
        setTimeout(function () {
            alert.hideAlert();
        }, 5000);
    };
    this.hideAlert = function () {
        this.$alert.hide();
    }
}