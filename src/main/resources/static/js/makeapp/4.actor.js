/**
 * Created by mkmaposa on 2016/05/30.
 */

var actor = new Actor();

var ACTOR_API_BASE_URL= $("#actor_api_base_url").val();
var ACTOR_BASE_URL = $("#actor_base_url").val();
var $alert = $('.alert');
var $table = $("#actor_list_table");

function Actor() {
    this.$actorModal = $('#add_actor_modal');
    // console.log("We are about to show the modal window");
    this.addNewActor = function() {
        // console.log("We are about to show the modal window");
        $('#add_actor_modal').modal('show');
    };
    this.save = function() {
        var actor = {};

        actor.firstName = $("#firstName").val();
        actor.lastName = $("#lastName").val();
        actor.gender = $("#gender").val();
        
        $.ajax({
            type: this.$actorModal.data('id') ? 'put' : 'post',
            url: ACTOR_API_BASE_URL + 'add/' + (this.$actorModal.data('id') || ''),
            data: JSON.stringify(actor),
            contentType: 'application/json',
            dataType: 'json',
            success: function (response) {
                if (response.status) {
                    refreshTable();
                }
                displayAlert(response, 'success');
            }
        });
    };
    this.showAlert = function (title, type) {
        $alert.attr('class', 'alert alert-' + type || 'success')
            .html('<i class="glyphicon glyphicon-check"></i> ' + title).show();
        setTimeout(function () {
            $alert.hide();
        }, 5000);
    };
    this.showModal = function (title, row) {
        this.$actorModal.data('id', row.id);
        this.$actorModal.find('.modal-title').text(title);
        for (var name in row) {
            this.$actorModal.find('input[name="' + name + '"]').val(row[name]);
        }
        this.$actorModal.modal('show');
    };
}

$(function () {
    $("#actor_list_table").bootstrapTable({
        url: ACTOR_API_BASE_URL + 'list',
        pagination: true,
        sortOrder: 'asc',
        pageSize: 25
    });

    $('#hover, #striped, #condensed').click(function () {
        var classes = 'table';

        if ($('#hover').prop('checked')) {
            classes += ' table-hover';
        }
        if ($('#condensed').prop('checked')) {
            classes += ' table-condensed';
        }
        $('#table-style').bootstrapTable('destroy')
            .bootstrapTable({
                classes: classes,
                striped: $('#striped').prop('checked')
            });
    });

    $(document).on('click', '#actor_add', function() {
        console.log("We have clicked actor add link");
        actor.addNewActor();
    });

    var $actorForm = $("#actor_form");
    $actorForm.validate({
        rules: {
            firstName: "required",
            lastName: "required",
            gender: "required"
        },
        messages: {
            first_name: "Please enter first name",
            last_name: "Please enter last name",
            gender: "Please enter the gender"
        }
    });

    $actorForm.on('submit', function(e) {
        e.preventDefault();

        if($actorForm.valid()) {
            actor.save();
        } else {
            // displayAlert("actor Form is not valid", 'danger');
        }
    });
});


function queryParams(params) {
    return {};
}
function actionFormatter(value) {
    return [
        '<a class="view" href="javascript:" title="View Item Details"><i class="glyphicon glyphicon-eye-open"></i></a>',
        '<a class="update" href="javascript:" title="Update Item"><i class="glyphicon glyphicon-edit"></i></a>',
        '<a class="remove" href="javascript:" title="Delete Item"><i class="glyphicon glyphicon-remove-circle"></i></a>',
    ].join('&nbsp;|&nbsp;');
}


// update and delete events
window.actionEvents = {
    'click .view': function (e, value, row) {
        var actorId = row["id"];
        window.location.href = ACTOR_BASE_URL + '/details/' + actorId;
    },
    'click .update': function (e, value, row) {
        actor.showModal($(this).attr('title'), row);
    },
    'click .remove': function (e, value, row) {
        if (confirm('Are you sure to delete this item?')) {
            $.ajax({
                url: ACTOR_API_BASE_URL + 'delete/' + row.id,
                type: 'delete',
                success: function (response) {
                    refreshTable();
                    displayAlert(response, 'success');
                },
                error: function (response) {
                    actor.showAlert(response.message, 'danger');
                }
            })
        }
    }
};

function displayAlert(response, type) {
    actor.$actorModal.modal('hide');
    actor.showAlert(response.message, type);
}

function refreshTable() {
    $table.bootstrapTable('refresh');
}