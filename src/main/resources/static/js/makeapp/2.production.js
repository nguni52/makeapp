/**
 * Created by nguni52 on 2016/05/25.
 */
var production = new Production();
var PROD_API_BASE_URL = $("#prod_api_base_url").val();
var PROD_BASE_URL = $("#prod_base_url").val();
var alert = new Alert();
var $table = $("#prod_list_table");
function Production() {
    this.$productionModal = $('#add_production_modal');
    this.$productionPackageOptionsmodal = $("#package_options");
    this.$creditCardModal = $("#credit_card_modal");
    
    this.addNewProduction = function () {
        this.$productionModal.modal('show');
    };
    this.save = function () {
        var production = {};
        production.name = $("#prod_name").val();

        var officeAddress = {};
        officeAddress.street1 = $("#street1").val();
        officeAddress.street2 = $("#street2").val();
        officeAddress.suburb = $("#suburb").val();
        officeAddress.city = $("#city").val();
        officeAddress.code = $("#code").val();
        officeAddress.province = $("#province").val();
        officeAddress.country = $("#country").val();

        production.officeAddress = officeAddress;

        production.tel = $("#tel").val();
        production.fax = $("#fax").val();
        production.execProducer = $("#execProducer").val();
        production.commEditor = $("#commEditor").val();
        production.creativeDirector = $("#creativeDirector").val();
        production.producer = $("#producer").val();
        production.director = $("#director").val();
        production.continuity = $("#continuity").val();
        production.lineProducer = $("#lineProducer").val();
        production.prodManager = $("#prodManager").val();
        production.prodCoordinator = $("#prodCoordinator").val();

        $.ajax({
            type: this.$productionModal.data('id') ? 'put' : 'post',
            url: PROD_API_BASE_URL + 'add/' + (this.$productionModal.data('id') || ''),
            data: JSON.stringify(production),
            contentType: 'application/json',
            dataType: 'json',
            success: function (response) {
                if (response.status) {
                    refreshTable();
                }
                displayAlert(response);
            }
        });
    };
    this.showModal = function (title, row) {
        this.$productionModal.data('id', row.id);
        this.$productionModal.find('.modal-title').text(title);
        this.$productionModal.find('input[name="prod_name"]').val(row.name);
        this.$productionModal.find('input[name="street1"]').val(row.officeAddress.street1);
        this.$productionModal.find('input[name="street2"]').val(row.officeAddress.street2);
        this.$productionModal.find('input[name="suburb"]').val(row.officeAddress.suburb);
        this.$productionModal.find('input[name="city"]').val(row.officeAddress.city);
        this.$productionModal.find('input[name="code"]').val(row.officeAddress.code);
        this.$productionModal.find('input[name="province"]').val(row.officeAddress.province);
        this.$productionModal.find('input[name="country"]').val(row.officeAddress.country);
        this.$productionModal.find('input[name="tel"]').val(row.tel);
        this.$productionModal.find('input[name="fax"]').val(row.fax);
        this.$productionModal.find('input[name="execProducer"]').val(row.execProducer);
        this.$productionModal.find('input[name="creativeDirector"]').val(row.creativeDirector);
        this.$productionModal.find('input[name="producer"]').val(row.producer);
        this.$productionModal.find('input[name="director"]').val(row.director);
        this.$productionModal.find('input[name="continuity"]').val(row.continuity);
        this.$productionModal.find('input[name="lineProducer"]').val(row.lineProducer);
        this.$productionModal.find('input[name="prodManager"]').val(row.prodManager);
        this.$productionModal.find('input[name="prodCoordinator"]').val(row.prodCoordinator);
        this.$productionModal.modal('show');
    };
    this.showAlert = function (title, type) {
        $alert.attr('class', 'alert alert-' + type || 'success')
            .html('<i class="glyphicon glyphicon-check"></i> ' + title).show();
        setTimeout(function () {
            $alert.hide();
        }, 5000);
    };
    this.showProductionPackages = function () {
        this.$productionPackageOptionsmodal.modal('show');
    };
    this.hideProductionPackages = function () {
        this.$productionPackageOptionsmodal.modal('hide');
    };
    this.showCreditCardModal = function () {
        this.$creditCardModal.modal('show');

        // get the packages from local storage
        var package_selected = store.get("package_selected");
        var packages = store.get("packages");
        var selectedPackageJSON;
        $.each(packages, function (i, item) {
            if (item.name === package_selected) {
                selectedPackageJSON = item;
                return;
            }
        });

        if (selectedPackageJSON !== null) {
            $("#packageName").text(selectedPackageJSON.name);
            $("#paymentAmount").val("R" + selectedPackageJSON.price);
        }
    };
    this.getPackages = function () {
        var packageUrl = $("#packageurl").val();
        $.ajax({
            url: packageUrl,
            type: 'get',
            dataType: 'json',
            success: function (data) {
                store.set("packages", data);
            },
            error: function () {

            }
        })
    }
}

$(function () {

    $table.bootstrapTable({
        url: PROD_API_BASE_URL + 'list',
        search: true,
        pagination: true,
        sortOrder: 'asc',
        pageSize: 25
    });

    $('#hover, #striped, #condensed').click(function () {
        var classes = 'table';

        if ($('#hover').prop('checked')) {
            classes += ' table-hover';
        }
        if ($('#condensed').prop('checked')) {
            classes += ' table-condensed';
        }
        $('#table-style').bootstrapTable('destroy')
            .bootstrapTable({
                classes: classes,
                striped: $('#striped').prop('checked')
            });
    });

    $(document).on('click', '#production_add', function () {
         production.addNewProduction();
        //production.showProductionPackages();
    });

    $(document).on('click', '.btn-black', function () {
        var $btnTitle = $(this).prop('title');
        if (confirm('Are you sure you want to select the ' + $btnTitle + ' package?')) {
            production.hideProductionPackages();
            // save the name of the package selected in local storage
            store.set("package_selected", $btnTitle);
            production.showCreditCardModal();
        }
    });

    production.getPackages();

    var $prodForm = $("#prod_form");
    $prodForm.validate({
        rules: {
            prod_name: "required",
            street1: "required",
            city: "required",
            code: "required",
            province: "required",
            country: "required",
            tel: "required",
            fax: "required",
            execProducer: "required",
            // creativeDirector: "required",
            // producer: "required",
            director: "required"//,
            // continuity: "required",
            // lineProducer: "required",
            // prodManager: "required",
            // prodCoordinator: "required"
        },
        messages: {
            prod_name: "Please enter the production name",
            street1: "Please enter the main street name",
            city: "Please enter the city",
            code: "Please enter the area code",
            province: "Please enter the province",
            country: "Please enter your country",
            tel: "Please enter the telephone number",
            fax: "Please enter the fax number",
            execProducer: "Please enter the name of the executive producer",
            director: "Please enter the name of the director"
        }
    });

    $prodForm.on('submit', function (e) {
        e.preventDefault();
        if ($(this).valid()) {
            production.save();
        }
    });
});

function queryParams(params) {
    return {};
}
function actionFormatter(value) {
    return [
        '<a class="view" href="javascript:" title="View Item Details"><i class="glyphicon glyphicon-eye-open"></i></a>',
        '<a class="update" href="javascript:" title="Update Item"><i class="glyphicon glyphicon-edit"></i></a>',
        '<a class="remove" href="javascript:" title="Delete Item"><i class="glyphicon glyphicon-remove-circle"></i></a>',
    ].join('&nbsp;|&nbsp;');
}
function actorListFormatter(value) {
    return ['<a class="view" href="javascript:" title="View Actor List">Actor List</a>'].join('&nbsp;|&nbsp;');
}

function userListFormatter(value) {
    return ['<a class="view" href="javascript:" title="View User List">User List</a>'].join('&nbsp;|&nbsp;');
}

// update and delete events
window.actionEvents = {
    'click .view': function (e, value, row) {
        var productionId = row["id"];
        window.location.href = PROD_BASE_URL + 'details/' + productionId;
    },
    'click .update': function (e, value, row) {
        production.showModal($(this).attr('title'), row);
    },
    'click .remove': function (e, value, row) {
        if (confirm('Are you sure to delete this item?')) {
            $.ajax({
                url: PROD_API_BASE_URL + 'delete/' + row.id,
                type: 'delete',
                success: function (response) {
                    refreshTable();
                    alert.showAlert(response.message, 'success');
                },
                error: function (response) {
                    alert.showAlert(response.message, 'danger');
                }
            })
        }
    }
};
window.actorListEvents = {
    'click .view': function (e, value, row) {
        var productionId = row["id"];
        window.location.href = PROD_BASE_URL + productionId + '/character';
    }
};

window.userListEvents = {
    'click .view': function (e, value, row) {
        var productionId = row["id"];
        window.location.href = PROD_BASE_URL + productionId + '/produser';
    }
};

function displayAlert(response) {
    production.$productionModal.modal('hide');
    alert.showAlert(response.message, 'success');
}

function refreshTable() {
    $table.bootstrapTable('refresh');
}

init();
function init() {
    if (!store.enabled) {
        alert('Local storage is not supported by your browser. Please disable "Private Mode", or upgrade to a modern browser.');
    }
}