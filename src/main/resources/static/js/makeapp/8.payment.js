/**
 * Created by nguni52 on 2016/10/03.
 */
$(function () {
    fillOutTestData();
    $("#processing-payment-btn").hide();
    var $ccForm = $("#myCCForm");
    $ccForm.find('fieldset').hide();
    $ccForm.find('fieldset:first').fadeIn('slow');
    $('#myCCForm input[type="text"], #myCCForm input[type="password"], #myCCForm textarea').on('focus', function () {
        $(this).removeClass('input-error');
    });

    // next step
    $ccForm.find('.btn-next').on('click', function () {
        var parent_fieldset = $(this).parents('fieldset');
        var next_step = true;

        parent_fieldset.find('input[type="text"], input[type="password"], textarea').each(function () {
            if ($(this).val() == "" && $(this).attr('name') != 'addressTwo') {
                $(this).addClass('input-error');
                next_step = false;
            }
            else {
                $(this).removeClass('input-error');
            }
        });
        console.log(next_step);

        if (next_step) {
            parent_fieldset.fadeOut(400, function () {
                $(this).next().fadeIn();
            });
        }

    });

    // previous step
    $ccForm.find('.btn-previous').on('click', function () {
        $(this).parents('fieldset').fadeOut(400, function () {
            $(this).prev().fadeIn();
        });
    });

    $ccForm.submit(function (e) {
        e.preventDefault();
        // disable the submit button
        var $processPaymentBtn = $("#process-payment-btn").hide();
        // show the loading indicator
        $("#processing-payment-btn").show();

        var paymentProcessingUrl = $("#payment_processing_url").val();
        var payData = {};
        var address = {};

        // get the address details
        address.line1 = $("#addressOne").val();
        address.line2 = $("#addressTwo").val();
        address.city = $("#address_city").val();
        address.countryCode = "ZA";
        address.postalCode = $("#address_code").val();
        address.state = $("#province").val();

        // get the credit card details
        var ccDetails = {};
        var ccNo = $("#ccNo").val();
        ccNo = ccNo.replace(/\s+/g, "");
        ccDetails.number = ccNo;
        ccDetails.type = "visa";
        var expiry = $("#expiry").val();
        var expiryArr = expiry.split("/");
        ccDetails.expiryMonth = expiryArr[0].trim();
        ccDetails.expiryYear = "20" + expiryArr[1].trim();
        ccDetails.cvv2 = $("#cvv").val().trim();
        // ccDetails.billingAddress = address;
        ccDetails.firstName = "Nguni";
        ccDetails.lastName = "Phakela";


        // get the current user
        var userReq = $.get($("#getcurrentuser_url").val())
            .done(function (username) {
                if (username) {
                    // assign the payment details to final object before send off
                    ccDetails.payerId = username;
                    payData.creditCard = ccDetails;
                    // payData.address = address;
                    var amount = $("#paymentAmount").val();
                    amount.replace(/R+/g, "");
                    payData.price = amount;
                    payData.cvv2 = $("#cvv").val().trim();

                    //contentType: 'application/json',
                    var req = $.ajax({
                        type: 'post',
                        url: paymentProcessingUrl,
                        headers: {
                            'Accept': 'application/json',
                            'Content-Type': 'application/json'
                        },
                        dataType: 'json',
                        data: JSON.stringify(payData),
                        success: function (data) {
                            console.log(data);
                            if (data.status) {
                                // show success message
                                $("#credit_card_modal").modal('hide');
                                $("#add_production_modal").modal('show');
                            }
                        },
                        error: function (data) {
                            console.log(data);
                        }
                    });
                } else {
                    console.log("we cannot retrieve the current user");
                }
            });


    });
});

var card = new Card({
    form: '#myCCForm',
    container: '.card-wrapper', // *required*
    formSelectors: {
        numberInput: 'input#ccNo', // optional — default input[name="number"]
        expiryInput: 'input#expiry', // optional — default input[name="expiry"]
        cvcInput: 'input#cvv', // optional — default input[name="cvc"]
        nameInput: 'input#ccName' // optional - defaults input[name="name"]
    },

    width: 250, // optional — default 350px
    formatting: true, // optional - default true

    // Strings for translation - optional
    messages: {
        validDate: 'valid\ndate', // optional - default 'valid\nthru'
        monthYear: 'mm/yyyy' // optional - default 'month/year'
    },

    // Default placeholders for rendered fields - optional
    placeholders: {
        number: '•••• •••• •••• ••••',
        name: 'Full Name',
        expiry: '••/••',
        cvc: '•••'
    },

    // if true, will log helpful messages for setting up Card
    debug: true // optional - default false
});


function fillOutTestData() {
    $("#addressOne").val("169 Rolbal Avenue");
    $("#addressTwo").val("Terrace Hill II");
    $("#address_city").val("Weltevredenpark");
    $("#province").val("Gauteng");
    $("#address_code").val("1709");
}