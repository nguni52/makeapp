/**
 * Created by nguni52 on 2016/06/01.
 */
var episode = new Episode();
var EPISODE_API_BASE_URL = $("#episode_api_base_url").val();
var EPISODE_BASE_URL = $("#episode_base_url").val();
var $alert = $('.alert');
var $table = $("#episode_list_table");

function Episode() {
    this.$episodeModal = $('#add_episode_modal');
    this.addNewEpisode = function () {
        this.$episodeModal.modal('show');
    };
    this.save = function () {
        var episode = {};

        episode.name = $("#name").val();
        episode.number = $("#number").val();
        episode.summary = $("#summary").val();
        episode.productionId = $("#productionId").val();

        $.ajax({
            type: this.$episodeModal.data('id') ? 'put' : 'post',
            url: EPISODE_API_BASE_URL + 'add/' + (this.$episodeModal.data('id') || ''),
            data: JSON.stringify(episode),
            contentType: 'application/json',
            dataType: 'json',
            success: function (response) {
                if (response.status) {
                    refreshTable();
                }
                displayAlert(response);
            }
        });
    };
    this.showAlert = function (title, type) {
        $alert.attr('class', 'alert alert-' + type || 'success')
            .html('<i class="glyphicon glyphicon-check"></i> ' + title).show();
        setTimeout(function () {
            $alert.hide();
        }, 5000);
    };
    this.showModal = function (title, row) {
        this.$episodeModal.data('id', row.id);
        this.$episodeModal.find('.modal-title').text(title);
        for (var name in row) {
            this.$episodeModal.find('input[name="' + name + '"]').val(row[name]);
        }
        this.$episodeModal.modal('show');
    };
}

$(function () {
    $table.bootstrapTable({
        url: EPISODE_API_BASE_URL + $("#productionId").val() + '/list',
        search: true,
        pagination: true,
        sortOrder: 'asc',
        pageSize: 25
    });

    $(document).on('click', '#episode_add', function () {
        episode.addNewEpisode();
    });

    var $episodeForm = $("#episode_form");
    $episodeForm.validate({
        rulea: {
            name: "required",
            number: "required",
            summary: "required"
        },
        messages: {
            name: "Please specify the episode name",
            number: "Please specify the episode number",
            summary: "Please specify the episode summary"
        }
    });
    $episodeForm.on('submit', function (e) {
        e.preventDefault();
        if ($(this).valid()) {
            episode.save();
        }
    });
});


function displayAlert(response) {
    episode.$episodeModal.modal('hide');
    episode.showAlert(response.message, 'success');
}

function refreshTable() {
    $table.bootstrapTable('refresh');
}

// update and delete events
window.actionEvents = {
    'click .view': function (e, value, row) {
        var episodeId = row["id"];
        window.location.href = EPISODE_BASE_URL + 'details/' + episodeId;
    },
    'click .update': function (e, value, row) {
        episode.showModal($(this).attr('title'), row);
    },
    'click .remove': function (e, value, row) {
        if (confirm('Are you sure to delete this item?')) {
            $.ajax({
                url: EPISODE_API_BASE_URL + 'delete/' + row.id,
                type: 'delete',
                success: function (response) {
                    refreshTable();
                    episode.showAlert(response.message, 'success');
                },
                error: function (response) {
                    episode.showAlert(response.message, 'danger');
                }
            })
        }
    }
};

function queryParams(params) {
    return {};
}
function actionFormatter(value) {
    return [
        '<a class="view" href="javascript:" title="View Item Details"><i class="glyphicon glyphicon-eye-open"></i></a>',
        '<a class="update" href="javascript:" title="Update Item"><i class="glyphicon glyphicon-edit"></i></a>',
        '<a class="remove" href="javascript:" title="Delete Item"><i class="glyphicon glyphicon-remove-circle"></i></a>',
    ].join('&nbsp;|&nbsp;');
}
