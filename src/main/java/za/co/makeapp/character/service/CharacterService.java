package za.co.makeapp.character.service;

import za.co.makeapp.data.model.Character;
import za.co.makeapp.data.model.ImageComment;
import za.co.makeapp.data.model.UploadImage;

import java.util.Collection;
import java.util.List;

/**
 * Created by nguni52 on 2016/06/20.
 */
public interface CharacterService {
    void create(Character character);
    Character findOne(String id);
    Collection<Character> findAllByProdId(String prodId);
    void update(Character character);
    void delete(String id);
    void uploadImage(UploadImage uploadImage, String sceneId, String characterId, String imageComment);
    List<String> findCharacterImagesForScene(String sceneId, String characterId);
    UploadImage findCharacterImage(String id);
    void deleteCharacterImage(String imageId);
    void saveImageComment(ImageComment imageComment);
    ImageComment getImageCommentByImageId(String id);
}
