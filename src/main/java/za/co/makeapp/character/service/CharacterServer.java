package za.co.makeapp.character.service;

import com.mongodb.BasicDBObject;
import com.mongodb.DBObject;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import za.co.makeapp.data.model.Character;
import za.co.makeapp.data.model.ImageComment;
import za.co.makeapp.data.model.UploadImage;
import za.co.makeapp.data.repository.CharacterRepository;
import za.co.makeapp.data.repository.ImageCommentRepository;
import za.co.makeapp.file.service.FileStorageService;
import za.co.makeapp.scene.service.SceneService;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

/**
 * Created by nguni52 on 2016/06/20.
 */
@Service
public class CharacterServer implements CharacterService {
    @Autowired
    private CharacterRepository characterRepository;
    @Autowired
    private FileStorageService fileStorageService;
    private Log log = LogFactory.getLog(this.getClass().getName());
    @Autowired
    private ImageCommentRepository imageCommentRepository;
    @Autowired
    private SceneService sceneService;

    @Override
    public void create(Character character) {
        throwExceptionIfCharacterExists(character);
        save(character);
    }

    private void throwExceptionIfCharacterExists(Character character) {
        Character existingCharacter = characterRepository.findByCharacterIdAndProductionId(character.getCharacterId(), character.getProductionId());
        if (existingCharacter != null) {
            throw new RuntimeException("Character with character id " + character.getCharacterId() + " already exists");
        }
    }

    private void save(Character character) {
        characterRepository.save(character);
    }

    @Override
    public Character findOne(String id) {
        return characterRepository.findOne(id);
    }

    @Override
    public Collection<Character> findAllByProdId(String prodId) {
        return characterRepository.findByProductionId(prodId);
    }

    @Override
    public void update(Character character) {
        save(character);
    }

    @Override
    public void delete(String id) {
        characterRepository.delete(id);
    }

    @Override
    public void uploadImage(UploadImage uploadImage, String sceneId, String characterId, String imageComment) {
        DBObject metaData = new BasicDBObject();
        metaData.put("sceneId", sceneId);
        metaData.put("characterId", characterId);
        metaData.put("comment", imageComment);
        fileStorageService.save(uploadImage.getInputStream(), uploadImage.getFileName(),
                uploadImage.getFileContentType(), metaData);
        //update the scene date shot after uploading the image to show when the scene was actually shot
        sceneService.setDateShot(sceneId);
    }

    @Override
    public List<String> findCharacterImagesForScene(String sceneId, String characterId) {
        List<UploadImage> images = fileStorageService.findCharacterImagesForScene(sceneId, characterId);
        List<String> imageIds = new ArrayList<>();
        for(UploadImage uploadImage: images) {
            imageIds.add(uploadImage.getId());
        }

        return imageIds;
    }

    @Override
    public UploadImage findCharacterImage(String id) {
        log.info("Image id in character server is: " + id);
        return fileStorageService.findOne(id);
    }

    @Override
    public void deleteCharacterImage(String imageId) {
        fileStorageService.delete(imageId);
    }

    @Override
    public void saveImageComment(ImageComment imageComment) {
        imageCommentRepository.save(imageComment);
    }

    @Override
    public ImageComment getImageCommentByImageId(String id) {
        UploadImage uploadImage = fileStorageService.findOne(id);
        log.info("medata is: " + uploadImage.getMetaData());
        String comment = uploadImage.getMetaData().get("comment").toString();
        return new ImageComment(id, comment);
    }
}
