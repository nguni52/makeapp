package za.co.makeapp.character.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import za.co.makeapp.api.APICharacterController;
import za.co.makeapp.api.MainController;
import za.co.makeapp.api.SceneAPIController;
import za.co.makeapp.character.service.CharacterService;
import za.co.makeapp.data.model.Character;
import za.co.makeapp.scene.controller.SceneController;

import java.util.List;

/**
 * Created by nguni52 on 11/22/16.
 */
@Controller
@RequestMapping(value = SceneCharacterController.CHARACTER)
public class SceneCharacterController extends MainController {

    public static final String CHARACTER = "character";
    private static final String CHARACTER_IMAGE_IDS = "characterImageIds";
    private static final String SCENE_ID = "sceneId";
    public static final String CHARACTER_INDEX = SceneController.SCENE + HOME + CHARACTER + HOME + INDEX;

    @Autowired
    private CharacterService characterService;

    @RequestMapping(method = RequestMethod.GET, value = APICharacterController.HOME_IMAGE_SCENE_ID)
    public String getCharacterImages(@PathVariable(ID) String sceneId,
                                     @PathVariable(SceneAPIController.CHARACTER_ID) String characterId,
                                     ModelMap model) {
        List<String> characterImageIds = characterService.findCharacterImagesForScene(sceneId, characterId);
        Character character = characterService.findOne(characterId);

        model.addAttribute(CHARACTER_IMAGE_IDS, characterImageIds);
        model.addAttribute(CHARACTER, character);
        model.addAttribute(SCENE_ID, sceneId);

        return CHARACTER_INDEX;
    }
}
