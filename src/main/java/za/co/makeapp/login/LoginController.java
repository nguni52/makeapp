package za.co.makeapp.login;

import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import za.co.makeapp.api.MainController;

/**
 * Created by nguni52 on 2016/05/25.
 */
@Controller
@RequestMapping(value=LoginController.HOME_LOGIN)
public class LoginController extends MainController {
    private static final String LOGIN = "login";
    public static final String HOME_LOGIN = HOME + LOGIN;

    @RequestMapping(method = RequestMethod.GET)
    public String getLogin(ModelMap model) {
        model.addAttribute(TITLE, LOGIN);

        return LOGIN;
    }
}
