package za.co.makeapp.login.service;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import za.co.makeapp.data.model.User;
import za.co.makeapp.user.service.UserService;

import java.util.ArrayList;
import java.util.Collection;

/**
 * Created by nguni52 on 2016/05/25.
 */
@Service
public class LoginService implements UserDetailsService {
    private Log log = LogFactory.getLog(this.getClass().getName());
    @Autowired
    private UserService userService;

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        User user = getUser(username);
        if (user == null) {
            throw new UsernameNotFoundException("user account not found");
        }
        return createUser(user);
    }

    private User getUser(String username) {
        return userService.findByUsername(username);
    }

    public org.springframework.security.core.userdetails.User createUser(User user) {
        org.springframework.security.core.userdetails.User springUser = new org.springframework.security.core.userdetails.User(user.getUsername(), user.getPassword(), createAuthority(user));

        return springUser;
    }

    public void login(User user) {
        SecurityContextHolder.getContext().setAuthentication(authenticate(user));
    }

    private Authentication authenticate(User user) {
        return new UsernamePasswordAuthenticationToken(createUser(user), null, createAuthority(user));
    }

    private Collection<GrantedAuthority> createAuthority(User user) {
        Collection<GrantedAuthority> authorities = new ArrayList<GrantedAuthority>();
        for (String role : user.getRole()) {
            authorities.add(new SimpleGrantedAuthority(role));
        }
        return authorities;
    }
}
