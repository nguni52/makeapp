package za.co.makeapp.reports.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import za.co.makeapp.api.MainController;

import static za.co.makeapp.scene.controller.SceneController.SCENE;

/**
 * Created by nguni52 on 2017/06/29.
 */
@Controller
@RequestMapping(value = ReportController.REPORT)
public class ReportController extends MainController {
    public static final String REPORT_INDEX = REPORT + HOME + INDEX;
    public static final String REPORT_SCENE_INDEX = REPORT + HOME + SCENE + HOME + INDEX;
    public static final String HOME_SCENE = HOME + SCENE;

    @RequestMapping(method = RequestMethod.GET)
    public String getMain(ModelMap model) {

        return REPORT_INDEX;
    }

    @RequestMapping(method =  RequestMethod.GET, value = HOME_SCENE)
    public String getSceneReports(ModelMap model) {
        return REPORT_SCENE_INDEX;
    }
}
