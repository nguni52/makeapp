package za.co.makeapp.production.service;

import za.co.makeapp.data.model.Character;
import za.co.makeapp.data.model.Production;
import za.co.makeapp.data.model.ProductionEmail;
import za.co.makeapp.data.model.Scene;

import java.util.Collection;
import java.util.List;

/**
 * Created by nguni52 on 2016/05/25.
 */
public interface ProductionService {
    void create(Production production);
    void save(Production production);
    List<Production> findAll();
    Production findOne(String id);
    void delete(String id);
    void update(Production production);
    Production findProductionBySceneId(String sceneId);

    Collection<Character> findNewCharactersByScene(Scene scene);
    List<ProductionEmail> findProductionsByEmail(String email);
    void createProductionEmail(ProductionEmail productionEmail);
    List<Production> findUserProductions(List<ProductionEmail> productionEmailList);
    List<ProductionEmail> findEmailsForProduction(String prodId);
    void deleteUserEmail(String id);
    void editProductionEmail(ProductionEmail productionEmail);
}
