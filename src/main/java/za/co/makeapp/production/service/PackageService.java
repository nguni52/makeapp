package za.co.makeapp.production.service;

import com.paypal.api.payments.*;
import com.paypal.base.rest.APIContext;
import com.paypal.base.rest.PayPalRESTException;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import za.co.makeapp.data.model.PaymentDetails;
import za.co.makeapp.data.model.ProductionPackage;
import za.co.makeapp.data.repository.PackageRepository;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by nguni52 on 2016/10/03.
 */
@Service
public class PackageService {
    @Autowired
    private PackageRepository packageRepository;
    @Value("${payment.paypal.client.id}")
    private String clientID;
    @Value("${payment.paypal.client.secret}")
    private String clientSecret;
    @Value("${payment.paypal.mode}")
    private String mode;
    private Log log = LogFactory.getLog(this.getClass().getName());

    public List<ProductionPackage> getPackages() {
        return packageRepository.findAll();
    }

    public Payment processPayment(PaymentDetails paymentDetails) {
        log.info("Payment Details are: " + paymentDetails.toString() + "\n\n");
        // ###Address
        // Base Address object used as shipping or billing
        // address in a payment. [Optional]
        Address billingAddress = new Address();
        billingAddress.setCity("Johnstown");
        billingAddress.setCountryCode("US");
        billingAddress.setLine1("52 N Main ST");
        billingAddress.setPostalCode("43210");
        billingAddress.setState("OH");

        // ###CreditCard
        // A resource representing a credit card that can be
        // used to fund a payment.
        CreditCard creditCard = new CreditCard();
        creditCard.setBillingAddress(billingAddress);
        creditCard.setCvv2(012);
        creditCard.setExpireMonth(11);
        creditCard.setExpireYear(2018);
        creditCard.setFirstName("Joe");
        creditCard.setLastName("Shopper");
        creditCard.setNumber("4669424246660779");
        creditCard.setType("visa");

        // ###Details
        // Let's you specify details of a payment amount.
        Details details = new Details();
        details.setShipping("0");
        details.setSubtotal("0");
        details.setTax("0");

        // ###Amount
        // Let's you specify a payment amount.
        Amount amount = new Amount();
        amount.setCurrency("USD");
        // Total must be equal to sum of shipping, tax and subtotal.
        amount.setTotal(paymentDetails.getPrice().replaceAll("R", ""));
        amount.setDetails(details);

        // ###Transaction
        // A transaction defines the contract of a
        // payment - what is the payment for and who
        // is fulfilling it. Transaction is created with
        // a `Payee` and `Amount` types
        Transaction transaction = new Transaction();
        transaction.setAmount(amount);
        transaction
                .setDescription("This is the payment transaction for a production package by " + paymentDetails.getCreditCard().getPayerId());

        // The Payment creation API requires a list of
        // Transaction; add the created `Transaction`
        // to a List
        List<Transaction> transactions = new ArrayList<Transaction>();
        transactions.add(transaction);

        // ###FundingInstrument
        // A resource representing a Payeer's funding instrument.
        // Use a Payer ID (A unique identifier of the payer generated
        // and provided by the facilitator. This is required when
        // creating or using a tokenized funding instrument)
        // and the `CreditCardDetails`
        FundingInstrument fundingInstrument = new FundingInstrument();
        fundingInstrument.setCreditCard(creditCard);

        // The Payment creation API requires a list of
        // FundingInstrument; add the created `FundingInstrument`
        // to a List
        List<FundingInstrument> fundingInstrumentList = new ArrayList<FundingInstrument>();
        fundingInstrumentList.add(fundingInstrument);

        // ###Payer
        // A resource representing a Payer that funds a payment
        // Use the List of `FundingInstrument` and the Payment Method
        // as 'credit_card'
        Payer payer = new Payer();
        payer.setFundingInstruments(fundingInstrumentList);
        payer.setPaymentMethod("credit_card");

        // ###Payment
        // A Payment Resource; create one using
        // the above types and intent as 'sale'
        Payment payment = new Payment();
        payment.setIntent("sale");
        payment.setPayer(payer);
        payment.setTransactions(transactions);
        Payment createdPayment = null;
        try {
            // ### Api Context
            // Pass in a `ApiContext` object to authenticate
            // the call and to send a unique request id
            // (that ensures idempotency). The SDK generates
            // a request id if you do not pass one explicitly.
            log.info("CLIENT ID: " + clientID);
            log.info("CLient SECRET: " + clientSecret);
            log.info("MODE: " + mode + "\n\n");
            APIContext apiContext = new APIContext(clientID, clientSecret, mode);
            // Create a payment by posting to the APIService
            // using a valid AccessToken
            // The return object contains the status;
            createdPayment = payment.create(apiContext);
            log.info("Created payment with id = " + createdPayment.getId()
                    + " and status = " + createdPayment.getState());
            log.info("Payment with Credit Card" +
                    Payment.getLastRequest() + ":: " + Payment.getLastResponse(), null);
        } catch (PayPalRESTException e) {
            throw new RuntimeException(e.getLocalizedMessage());

//            ResultPrinter.addResult(req, resp, "Payment with Credit Card",
//                    Payment.getLastRequest(), null, e.getMessage());
        }
        return createdPayment;
    }
}
