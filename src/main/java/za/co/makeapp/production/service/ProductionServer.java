package za.co.makeapp.production.service;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.joda.time.DateTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import za.co.makeapp.character.service.CharacterService;
import za.co.makeapp.data.model.*;
import za.co.makeapp.data.model.Character;
import za.co.makeapp.data.repository.ProductionEmailRepository;
import za.co.makeapp.data.repository.ProductionRepository;
import za.co.makeapp.episode.service.EpisodeService;
import za.co.makeapp.scene.service.SceneService;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

/**
 * Created by nguni52 on 2016/05/25.
 */
@Service
public class ProductionServer implements ProductionService {
    @Autowired
    private ProductionRepository productionRepository;
    @Autowired
    private ProductionEmailRepository productionEmailRepository;
    @Autowired
    private SceneService sceneService;
    @Autowired
    private EpisodeService episodeService;
    @Autowired
    private CharacterService characterService;
    private Log log = LogFactory.getLog(this.getClass().getName());

    @Override
    public void create(Production production) {
        throwExceptionIfProductionWithNameExists(production);
        production.setCreationDate(new DateTime());
        save(production);
    }

    @Override
    public void save(Production production) {
        productionRepository.save(production);
    }

    @Override
    public List<Production> findAll() {
        return productionRepository.findAll();
    }

    @Override
    public Production findOne(String id) {
        return productionRepository.findOne(id);
    }

    @Override
    public void delete(String id) {
        productionRepository.delete(id);
    }

    @Override
    public void update(Production production) {
        productionRepository.save(production);
    }

    @Override
    public Production findProductionBySceneId(String sceneId) {
        // get scene
        Scene scene = sceneService.findOne(sceneId);
        // get Episode using episode id from scene
        Episode episode = episodeService.findOne(scene.getEpisodeId());

        return productionRepository.findOne(episode.getProductionId());
    }


    /**
     * This method only shows the characters that have not yet been added to a scene. It does not show characters that
     * have already been added to a scene. We only want to add new characters, and not the ones that are part of a scene.
     *
     * @param scene - Scene details.
     * @return
     */
    @Override
    public Collection<Character> findNewCharactersByScene(Scene scene) {
        Production production = findProductionBySceneId(scene.getId());
        Collection<Character> characters = characterService.findAllByProdId(production.getId());
        ArrayList<Character> newCharacters = new ArrayList<>();
        boolean found = false;

        for (Character character : characters) {
            try {
                for (Character sceneCharacters : scene.getCharacters()) {
                    if (character.getId().equalsIgnoreCase(sceneCharacters.getId())) {
                        found = true;
                    }
                }
            } catch (Exception ex) {
                log.info("The scene does not have any characters");
            }
            if (!found) {
                newCharacters.add(character);
            }

            found = false;
        }

        return newCharacters;
    }

    @Override
    public List<ProductionEmail> findProductionsByEmail(String email) {
        return productionEmailRepository.findByEmail(email);
    }

    @Override
    public void createProductionEmail(ProductionEmail productionEmail) {
        productionEmailRepository.save(productionEmail);
    }

    @Override
    public List<Production> findUserProductions(List<ProductionEmail> productionEmailList) {
        List<Production> userProductions = new ArrayList<Production>();

        for(int i=0; i<productionEmailList.size(); i++) {
            Production production = this.findOne(productionEmailList.get(i).getProductionId());
            userProductions.add(i, production);
        }

        return userProductions;
    }

    @Override
    public List<ProductionEmail> findEmailsForProduction(String prodId) {
        return productionEmailRepository.findByProductionId(prodId);
    }

    @Override
    public void deleteUserEmail(String id) {
        productionEmailRepository.delete(id);
    }

    @Override
    public void editProductionEmail(ProductionEmail productionEmail) {
        productionEmailRepository.save(productionEmail);
    }

    private void throwExceptionIfProductionWithNameExists(Production production) {
        Production existingProduction = productionRepository.findByName(production.getName());
        if(existingProduction != null) {
            throw new RuntimeException("Production with name " + production.getName() + " already exists");
        }
    }
}
