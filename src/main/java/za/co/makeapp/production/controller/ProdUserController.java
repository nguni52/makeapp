package za.co.makeapp.production.controller;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import za.co.makeapp.api.MainController;

/**
 * Created by nguni52 on 2016/06/20.
 */
@Controller
@RequestMapping(value = ProdUserController.HOME_USER)
public class ProdUserController extends MainController {
    private Log log = LogFactory.getLog(this.getClass().getName());
    public static final String PRODUSER = "produser";
    private static final String PRODID = "prodId";
    public static final String HOME_USER = HOME + ProductionController.PRODUCTION + HOME + "{" + PRODID + "}" + HOME + PRODUSER;
    private static final String PRODUCTION_PRODUSER_INDEX = ProductionController.PRODUCTION + HOME + PRODUSER + HOME + INDEX;

    @RequestMapping(method = RequestMethod.GET)
    public String getProduser(@PathVariable(PRODID) String prodId, ModelMap model) {
        // get a list of actors/characters for this particular production
        //.characterService.findAll(prodId);

        log.info("Production ID: " + prodId + "\n\n\n");
        model.addAttribute(TITLE, PRODUSER);
        model.addAttribute(PRODID, prodId);

        return PRODUCTION_PRODUSER_INDEX;
    }
}
