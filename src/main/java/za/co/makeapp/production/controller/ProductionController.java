package za.co.makeapp.production.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import za.co.makeapp.api.MainController;
import za.co.makeapp.data.model.Production;
import za.co.makeapp.production.service.ProductionService;

/**
 * Created by nguni52 on 2016/05/25.
 */

@Controller
@RequestMapping(value = ProductionController.PRODUCTION)
public class ProductionController extends MainController {
    public static final String PRODUCTION = "production";
    private static final String PRODUCTION_INDEX = PRODUCTION + HOME + INDEX;

    private static final String PRODUCTION_ADD = PRODUCTION + HOME + ADD;

    private static final String PRODUCTION_DETAILS = PRODUCTION + HOME + DETAILS;
    private static final String HOME_DETAILS_ID = HOME + DETAILS + HOME + "{" + ID + "}";
    public static final String PRODID = "prodid";
    @Autowired
    private ProductionService productionService;


    @RequestMapping(method = RequestMethod.GET)
    public String getProduction(ModelMap model) {
        model.addAttribute(TITLE, PRODUCTION);

        return PRODUCTION_INDEX;
    }

    @RequestMapping(method = RequestMethod.GET, value=HOME_ADD)
    public String addProduction(ModelMap model) {
        model.addAttribute(TITLE, PRODUCTION_ADD);

        return PRODUCTION_ADD;
    }

    @RequestMapping(method = RequestMethod.GET, value = HOME_DETAILS_ID)
    public String showDetailsId(@PathVariable(ID) String id,
                                ModelMap model) {
        Production production = productionService.findOne(id);

        model.addAttribute(PRODUCTION, production);

        return PRODUCTION_DETAILS;
    }
}
