package za.co.makeapp.scene.service;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import za.co.makeapp.data.model.Character;
import za.co.makeapp.data.model.Scene;
import za.co.makeapp.data.repository.SceneRepository;
import za.co.makeapp.character.service.CharacterService;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.*;

/**
 * Created by mkmaposa on 2016/06/02.
 */

@Service
public class SceneServer implements SceneService {
    private Log log = LogFactory.getLog(SceneServer.class);
    @Autowired
    SceneRepository sceneRepository;
    @Autowired
    private CharacterService characterService;

    @Override
    public void create(Scene scene) {
        //throwExceptionIfSceneExists(scene);
        scene.setCreationDate(new DateTime());
        log.info("SHOOTING DATE I "+scene.getShootingDate());
        log.debug("SHOOTING DATE D "+scene.getShootingDate());
        save(scene);
    }

    private void throwExceptionIfSceneExists(Scene scene) {
        Scene existingScene = findByNumber(scene.getNumber());
        if(existingScene != null) {
            throw new RuntimeException("Scene, " + scene.getNumber() + ",  already exists");
        }
    }

    @Override
    public void save(Scene scene) {
        sceneRepository.save(scene);
    }

    @Override
    public Collection<Scene> findAll(String episodeId) {
        return sceneRepository.findByEpisodeId(episodeId);
    }

    @Override
    public void update(Scene scene) {
        this.save(scene);
    }

    @Override
    public void delete(String id) {
        sceneRepository.delete(id);
    }

    @Override
    public Scene findByNumber(String number) {
        return sceneRepository.findByNumber(number);
    }

    @Override
    public Scene findOne(String id) {
        return sceneRepository.findOne(id);
    }

    @Override
    public void saveCharacters(String sceneId, String[] characterIds) {
        Scene scene = findOne(sceneId);
        log.info(scene.toString());
        List<Character> characters = scene.getCharacters();
        if (characters == null) {
            characters = new ArrayList<>();
        }
        for (String characterId : characterIds) {
            Character character = characterService.findOne(characterId);
            log.info(character.toString());
            characters.add(character);
        }
        scene.setCharacters(characters);
        this.save(scene);
    }

    @Override
    public void deleteCharacterFromScene(String sceneId, String characterId) {
        Scene scene = findOne(sceneId);
        log.info("Scene: " + scene.toString());
        List<Character> newCharacters = new ArrayList<>();
        for (Character character : scene.getCharacters()) {
            if (!characterId.equalsIgnoreCase(character.getId())) {
                newCharacters.add(character);
            }
        }
        scene.setCharacters(newCharacters);
        save(scene);
    }

    @Override
    public void addSceneToReport(String sceneId){
        Scene scene = findOne(sceneId);
        scene.setHasBeenShot(true);
        scene.setDateShot(DateTime.now());
        update(scene);
    }

    @Override
    public List<Scene> getScenesByDateShot() {
//        return sceneRepository.findByDateShot();
        return null;

    }

    @Override
    public void setDateShot(String sceneId) {
        Scene scene = findOne(sceneId);
        scene.setDateShot(DateTime.now());
        scene.setHasBeenShot(true);
        update(scene);
    }
}
