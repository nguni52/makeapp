package za.co.makeapp.scene.service;

/**
 * Created by mkmaposa on 2016/06/02.
 */

import za.co.makeapp.data.model.Scene;

import java.util.Collection;
import java.util.List;

public interface SceneService {
    void create(Scene actor);
    void save(Scene actor);
    Collection<Scene> findAll(String episodeId);
    void update(Scene scene);
    void delete(String id);
    Scene findByNumber(String number);
    Scene findOne(String id);
    void saveCharacters(String sceneId, String[] characterIds);
    void deleteCharacterFromScene(String sceneId, String characterId);
    void addSceneToReport(String sceneId);
    List<Scene> getScenesByDateShot();
    void setDateShot(String sceneId);
}
