package za.co.makeapp.scene.controller;

/**
 * Created by mkmaposa on 2016/06/02.
 */

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import za.co.makeapp.api.MainController;
import za.co.makeapp.data.model.Character;
import za.co.makeapp.data.model.Scene;
import za.co.makeapp.production.controller.CharacterController;
import za.co.makeapp.production.service.ProductionService;
import za.co.makeapp.scene.service.SceneService;

import java.util.Collection;

@Controller
@RequestMapping(value = SceneController.SCENE)
public class SceneController extends MainController {
    public static final String SCENE = "scene";
    private static final String SCENE_DETAILS = SCENE + HOME + DETAILS;
    private static final String HOME_DETAILS = HOME + DETAILS + HOME + "{" + ID + "}";
    private static final String CHARACTERS = CharacterController.CHARACTER + "s";
    @Autowired
    private SceneService sceneService;
    @Autowired
    private ProductionService productionService;


    @RequestMapping(method = RequestMethod.GET, value = HOME_DETAILS)
    public String showDetailsId(@PathVariable(ID) String sceneId,
                                ModelMap model) {
        // get the scene
        Scene scene = sceneService.findOne(sceneId);
        Collection<Character> characters = productionService.findNewCharactersByScene(scene);

        model.addAttribute(SCENE, scene);
        model.addAttribute(CHARACTERS, characters);

        return SCENE_DETAILS;
    }
}
