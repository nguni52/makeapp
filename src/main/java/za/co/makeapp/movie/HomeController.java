package za.co.makeapp.movie;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import za.co.makeapp.api.MainController;
import za.co.makeapp.production.controller.ProductionController;

/**
 * Created by nguni52 on 2016/06/20.
 */
@Controller
@RequestMapping(value = MainController.HOME)
public class HomeController extends MainController {

    @RequestMapping(method = RequestMethod.GET)
    public String home() {
        return "redirect:" + HOME + ProductionController.PRODUCTION;
    }
}
