package za.co.makeapp.actor.controller;

/**
 * Created by mkmaposa on 2016/05/30.
 */

import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import za.co.makeapp.api.MainController;

@Controller
@RequestMapping(value = ActorController.ACTOR)
public class ActorController extends MainController{
    public static final String ACTOR = "actor";
    private static final String ACTOR_INDEX = ACTOR + HOME + INDEX;

    private static final String ACTOR_ADD = ACTOR + HOME + ADD;

    @RequestMapping(method = RequestMethod.GET)
    public String getActor(ModelMap model) {
        model.addAttribute(TITLE, ACTOR);

        return ACTOR_INDEX;
    }

    @RequestMapping(method = RequestMethod.GET, value=HOME_ADD)
    public String addActor(ModelMap model) {
        model.addAttribute(TITLE, ACTOR_ADD);

        return ACTOR_ADD;
    }
}
