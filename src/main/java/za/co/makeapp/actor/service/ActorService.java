package za.co.makeapp.actor.service;

/**
 * Created by nguni52 on 16/04/25.
 */

import za.co.makeapp.data.model.Actor;
import java.util.Collection;

public interface ActorService {
    void create(Actor actor);
    void save(Actor actor);
    Collection<Actor> findAll();
    void update(Actor actor);
    void delete(String id);
    Actor findByActorName(String firstName, String lastName);
    Actor findOne(String id);
}
