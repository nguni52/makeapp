package za.co.makeapp.actor.service;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import za.co.makeapp.data.model.Actor;
import za.co.makeapp.data.repository.ActorRepository;

import java.util.Collection;

/**
 * Created by nguni52 on 16/04/25.
 */
@Service
public class ActorServer implements ActorService {
    private Log log = LogFactory.getLog(ActorServer.class);
    @Autowired
    ActorRepository actorRepository;

    @Override
    public void create(Actor actor) {
        throwExceptionIfActorExists(actor);
        save(actor);
    }

    private void throwExceptionIfActorExists(Actor actor) {
        Actor existingActor = findByActorName(actor.getFirstName(), actor.getLastName());
        if(existingActor != null) {
            throw new RuntimeException("Actor, " + actor.getFirstName() + " " + actor.getLastName() + ",  already exists");
        }
    }

    @Override
    public void save(Actor actor) {
        actorRepository.save(actor);
    }

    @Override
    public Collection<Actor> findAll() {
        return actorRepository.findAll();
    }

    @Override
    public void update(Actor actor) {
        save(actor);
    }

    @Override
    public void delete(String id) {
        actorRepository.delete(id);
    }

    @Override
    public Actor findByActorName(String firstName, String lastName) {
        return actorRepository.findByFirstNameAndLastName(firstName, lastName);
    }

    @Override
    public Actor findOne(String id) {
        return actorRepository.findOne(id);
    }
}
