package za.co.makeapp.download;

import org.apache.commons.io.FilenameUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.propertyeditors.StringArrayPropertyEditor;
import org.springframework.core.io.FileSystemResource;
import org.springframework.core.io.InputStreamResource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;

/**
 * Created by nguni52 on 11/22/16.
 */
@Controller
public class DownloadController {

    private final String VERSION = "version";
    private final String ANDROID_DOWNLOAD_VERSION = "/android/download";
    private final String ANDROID_APK_LOCATION = "/usr/local/makeapp/makeapp"; //"/opt/android/makeapp-"
    private Log log = LogFactory.getLog(this.getClass().getName());

    @RequestMapping(value = ANDROID_DOWNLOAD_VERSION, method = RequestMethod.GET, produces="application/apk")
    public ResponseEntity<InputStreamResource> download() throws IOException {
        String fileLocation = ANDROID_APK_LOCATION + ".apk";
        log.info("File location: " + fileLocation);
        File file = new File(fileLocation);
        if (!file.exists()) {
            file = new File("/opt/code/makeapp/docs/makeapp.apk");
            if(!file.exists()) {
                throw new FileNotFoundException("Oops! File not found");
            }
        }

        InputStreamResource isResource = new InputStreamResource(new FileInputStream(file));
        FileSystemResource fileSystemResource = new FileSystemResource(file);
        String fileName = FilenameUtils.getName(file.getAbsolutePath());
        fileName=new String(fileName.getBytes("UTF-8"),"iso-8859-1");
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_OCTET_STREAM);
        headers.add("Cache-Control", "no-cache, no-store, must-revalidate");
        headers.add("Pragma", "no-cache");
        headers.add("Expires", "0");
        headers.setContentLength(fileSystemResource.contentLength());
        headers.setContentDispositionFormData("attachment", fileName);
        return new ResponseEntity<>(isResource, headers, HttpStatus.OK);
    }
}
