package za.co.makeapp.error;

import org.apache.log4j.Logger;
import org.springframework.web.HttpRequestMethodNotSupportedException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.NoHandlerFoundException;

import javax.servlet.http.HttpServletRequest;

/**
 * Created by nguni52 on 11/22/16.
 */
@ControllerAdvice
public class DefaultExceptionHandler {

    public static final String REQUEST_NOT_SUPPORTED = "405";
    public static final String RESOURCE_NOT_FOUND = "404";
    private Logger logger = Logger.getLogger(this.getClass().getName());

    @ExceptionHandler(value = HttpRequestMethodNotSupportedException.class)
    public ModelAndView methodNotSupportErrorHandler(HttpServletRequest req, Exception e) throws Exception {
        logger.error("Request: " + req.getRequestURL() + " raised " + e);

        ModelAndView mav = new ModelAndView();
        mav.addObject("exception", e);
        mav.addObject("url", req.getRequestURL());
        mav.setViewName(REQUEST_NOT_SUPPORTED);

        return mav;
    }

    @ExceptionHandler(NoHandlerFoundException.class)
    public ModelAndView handle404Error(HttpServletRequest request, Exception e) {

        logger.error("Request: " + request.getRequestURL() + " raised " + e);

        ModelAndView mav = new ModelAndView();
        mav.addObject("exception", e);
        mav.addObject("url", request.getRequestURL());
        mav.setViewName(RESOURCE_NOT_FOUND);

        return mav;
    }
}
