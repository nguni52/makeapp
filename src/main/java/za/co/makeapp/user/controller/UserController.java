package za.co.makeapp.user.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import za.co.makeapp.api.MainController;

/**
 * Created by nguni52 on 2016/06/27.
 */
@Controller
@RequestMapping(value = UserController.USER)
public class UserController extends MainController {
    public static final String USER = "user";
    private static final String USER_INDEX = USER + HOME + INDEX;

    @RequestMapping(method = RequestMethod.GET)
    public String getUser(ModelMap model) {

        return USER_INDEX;
    }
}
