package za.co.makeapp.user.service;

import com.mongodb.BasicDBObject;
import com.mongodb.DBObject;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import za.co.makeapp.data.model.ProfilePic;
import za.co.makeapp.data.model.User;
import za.co.makeapp.data.repository.UserRepository;
import za.co.makeapp.file.service.FileStorageServer;
import za.co.makeapp.file.service.FileStorageService;

import java.util.Collection;

/**
 * Created by nguni52 on 16/04/25.
 */
@Service
public class UserServer implements UserService {
    @Autowired
    UserRepository userRepository;
    private Log log = LogFactory.getLog(UserServer.class);
    @Autowired
    private FileStorageService fileStorageService;

    @Override
    public void create(User user) {
        throwExceptionIfUserExists(user);
        save(user);
    }

    @Override
    public void save(User user) {
        user.setPassword(encryptPassword(user.getPassword()));
        userRepository.save(user);
    }

    private String encryptPassword(String password) {
        BCryptPasswordEncoder encoder = new BCryptPasswordEncoder();
        return encoder.encode(password);
    }

    private void throwExceptionIfUserExists(User user) {
        User existingUser = findByUsername(user.getUsername());
        if(existingUser != null) {
            throw new RuntimeException("Username, " + user.getUsername() + ",  already exists");
        }
    }

    @Override
    public User findByUsername(String username) {
        return userRepository.findOne(username);
    }

    @Override
    public Collection<User> findAll() {
        return userRepository.findAll();
    }

    @Override
    public void update(User user) {
        User existingUser = userRepository.findOne(user.getUsername());

        if (user.getPassword() != null) {
            user.setPassword(encryptPassword(user.getPassword()));
        } else {
            user.setPassword(existingUser.getPassword());
        }
        save(user);
    }

    @Override
    public void delete(String username) {
        userRepository.delete(username);
    }

    @Override
    public User loginUser(User user) {
        User existingUserAccount = this.findByUsername(user.getUsername());
        log.info("Existing user is: " + existingUserAccount.toString());
        if (existingUserAccount != null) {
            BCryptPasswordEncoder passwordEncoder = new BCryptPasswordEncoder();
            if (passwordEncoder.matches(user.getPassword(), existingUserAccount.getPassword())) {
                return existingUserAccount;
            } else {
                throw new RuntimeException("Incorrect username/password combination");
            }
        } else {
            throw new RuntimeException("Incorrect username/password combination");
        }
    }

    @Override
    public void deleteAll() {
        userRepository.deleteAll();
    }

    @Override
    public boolean checkPassword(String username, String rawPassword) {
        BCryptPasswordEncoder encoder = new BCryptPasswordEncoder();
        User user = this.findByUsername(username);

        return encoder.matches(rawPassword, user.getPassword());
    }

    @Override
    public void changePassword(String username, String rawPassword) {
        User user = this.findByUsername(username);
        user.setPassword(rawPassword);
        this.save(user);
    }

    @Override
    public void uploadProfilePic(String username, ProfilePic proPicFile) {
        DBObject metaData = new BasicDBObject();
        metaData.put("username", proPicFile.getUsername());
        metaData.put("imageType", "propic");
        String proPicId = fileStorageService.save(proPicFile.getInputStream(), proPicFile.getFileName(), proPicFile.getFileContentType(), metaData);
        this.saveUserProPicId(username, proPicId);
    }

    @Override
    public ProfilePic getProPicByUsername(String username) throws Exception {
        User user = this.findByUsername(username);
        return fileStorageService.getByUsernameAndId(user.getUsername(), user.getProPicId());
    }

    private void saveUserProPicId(String username, String proPicId) {
        User user = this.findByUsername(username);
        user.setProPicId(proPicId);
        this.save(user);
    }
}
