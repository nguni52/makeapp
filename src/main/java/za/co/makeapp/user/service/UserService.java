package za.co.makeapp.user.service;

import za.co.makeapp.data.model.ProfilePic;
import za.co.makeapp.data.model.User;

import java.util.Collection;

/**
 * Created by nguni52 on 16/04/25.
 */
public interface UserService {
    void create(User user);
    void save(User user);
    User findByUsername(String username);
    Collection<User> findAll();
    void update(User user);
    void delete(String username);
    User loginUser(User user);
    void deleteAll();
    boolean checkPassword(String username, String rawPassword);
    void changePassword(String username, String rawPassword);
    void uploadProfilePic(String username, ProfilePic proPicFile);
    ProfilePic getProPicByUsername(String username) throws Exception;
}
