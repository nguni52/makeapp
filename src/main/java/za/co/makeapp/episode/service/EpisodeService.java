package za.co.makeapp.episode.service;

import za.co.makeapp.data.model.Episode;

import java.util.List;

/**
 * Created by nguni52 on 2016/06/01.
 */
public interface EpisodeService {
    void create(Episode episode);
    void update(Episode episode);

    List<Episode> findAll(String prodid);
    Episode findOne(String id);
    void delete(String id);
    void deleteAll();
}
