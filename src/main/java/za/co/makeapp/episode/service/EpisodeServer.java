package za.co.makeapp.episode.service;

import org.joda.time.DateTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import za.co.makeapp.data.model.Episode;
import za.co.makeapp.data.repository.EpisodeRepository;

import java.util.List;

/**
 * Created by nguni52 on 2016/06/01.
 */
@Service
public class EpisodeServer implements EpisodeService {
    @Autowired
    private EpisodeRepository episodeRepository;

    @Override
    public void create(Episode episode) {
        throwExceptionIfEpisodeWithNameExists(episode);
        episode.setCreationDate(new DateTime());
        save(episode);
    }

    private void save(Episode episode) {
        episodeRepository.save(episode);
    }

    private void throwExceptionIfEpisodeWithNameExists(Episode episode) {
        Episode existingEpisode = episodeRepository.findByNameAndProductionId(episode.getName(), episode.getProductionId());;
        if(existingEpisode != null) {
            throw new RuntimeException("Episode with name " + episode.getName() + " and production id " + episode.getProductionId() +
            " already exists!");
        }
    }

    @Override
    public void update(Episode episode) {
        this.save(episode);
    }

    @Override
    public List<Episode> findAll(String prodId) {
        return episodeRepository.findByProductionId(prodId);
    }

    @Override
    public Episode findOne(String id) {
        return episodeRepository.findOne(id);
    }

    @Override
    public void delete(String id) {
        episodeRepository.delete(id);
    }

    @Override
    public void deleteAll() {
        episodeRepository.deleteAll();
    }
}
