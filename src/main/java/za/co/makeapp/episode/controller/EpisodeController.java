package za.co.makeapp.episode.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import za.co.makeapp.api.MainController;
import za.co.makeapp.data.model.Episode;
import za.co.makeapp.episode.service.EpisodeService;

/**
 * Created by nguni52 on 2016/06/01.
 */
@Controller
@RequestMapping(value = EpisodeController.EPISODE)
public class EpisodeController extends MainController{
    public static final String EPISODE = "episode";
    private static final String EPISODE_INDEX = EPISODE + HOME + INDEX;
    private static final String EPISODE_DETAILS = EPISODE + HOME + DETAILS;
    private static final String HOME_DETAILS_ID = HOME + DETAILS + HOME + "{" + ID + "}";
    public static final String EPISODEID = "episodeid";

    @Autowired
    private EpisodeService episodeService;

    @RequestMapping(method = RequestMethod.GET)
    public String getEpisode(ModelMap model) {
        model.addAttribute(TITLE, EPISODE);

        return EPISODE_INDEX;
    }

    @RequestMapping(method = RequestMethod.GET, value = HOME_DETAILS_ID)
    public String showDetailsId(@PathVariable(ID) String id,
                                ModelMap model) {
        Episode episode = episodeService.findOne(id);

        model.addAttribute(EPISODE, episode);

        return EPISODE_DETAILS;
    }
}
