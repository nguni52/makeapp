package za.co.makeapp.data.repository;

import org.springframework.data.mongodb.repository.MongoRepository;
import za.co.makeapp.data.model.Episode;

import java.util.List;

/**
 * Created by nguni52 on 2016/06/01.
 */
public interface EpisodeRepository extends MongoRepository<Episode, String> {
    Episode findByNameAndProductionId(String name, String productionId);

    List<Episode> findByProductionId(String prodId);
}
