package za.co.makeapp.data.repository;

import org.springframework.data.mongodb.repository.MongoRepository;
import za.co.makeapp.data.model.Scene;

import java.util.Collection;

/**
 * Created by mkmaposa on 2016/06/02.
 */
public interface SceneRepository extends MongoRepository<Scene, String> {
    Scene findByNumber(String number);

    Collection<Scene> findByEpisodeId(String episodeId);
}
