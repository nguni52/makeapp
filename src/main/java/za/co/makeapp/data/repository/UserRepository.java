package za.co.makeapp.data.repository;

import org.springframework.data.mongodb.repository.MongoRepository;
import za.co.makeapp.data.model.User;

/**
 * Created by nguni52 on 16/04/25.
 */
public interface UserRepository extends MongoRepository<User, String>{
    User findByFirstNameAndLastName(String firstName, String lastName);
}
