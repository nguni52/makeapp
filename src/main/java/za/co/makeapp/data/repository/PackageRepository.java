package za.co.makeapp.data.repository;

import org.springframework.data.mongodb.repository.MongoRepository;
import za.co.makeapp.data.model.ProductionPackage;

/**
 * Created by nguni52 on 2016/10/03.
 */
public interface PackageRepository extends MongoRepository<ProductionPackage, String> {
}
