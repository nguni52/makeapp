package za.co.makeapp.data.repository;

import org.springframework.data.mongodb.repository.MongoRepository;
import za.co.makeapp.data.model.Production;

/**
 * Created by nguni52 on 2016/05/25.
 */
public interface ProductionRepository extends MongoRepository<Production, String> {
    Production findByName(String name);
}
