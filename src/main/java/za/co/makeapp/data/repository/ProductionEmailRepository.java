package za.co.makeapp.data.repository;

/**
 * Created by mkmaposa on 2017/03/13.
 */
import org.springframework.data.mongodb.repository.MongoRepository;
import za.co.makeapp.data.model.ProductionEmail;

import java.util.List;

public interface ProductionEmailRepository extends MongoRepository<ProductionEmail, String>{
    List<ProductionEmail> findByEmail(String email);
    List<ProductionEmail> findByProductionId(String prodId);
}
