package za.co.makeapp.data.repository;

import org.springframework.data.mongodb.repository.MongoRepository;
import za.co.makeapp.data.model.Actor;

/**
 * Created by nguni52 on 16/04/25.
 */
public interface ActorRepository extends MongoRepository<Actor, String> {
    Actor findByFirstNameAndLastName(String firstName, String lastName);
}
