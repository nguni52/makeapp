package za.co.makeapp.data.repository;

import org.springframework.data.mongodb.repository.MongoRepository;
import za.co.makeapp.data.model.Character;

import java.util.Collection;

/**
 * Created by nguni52 on 2016/06/20.
 */
public interface CharacterRepository extends MongoRepository<Character, String> {
    Collection<Character> findByProductionId(String prodId);

    Character findByCharacterIdAndProductionId(String characterId, String productionId);
}
