package za.co.makeapp.data.repository;

import org.springframework.data.mongodb.repository.MongoRepository;
import za.co.makeapp.data.model.ImageComment;

/**
 * Created by nguni52 on 2017/03/31.
 */
public interface ImageCommentRepository extends MongoRepository<ImageComment, String>{
}
