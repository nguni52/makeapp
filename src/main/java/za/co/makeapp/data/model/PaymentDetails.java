package za.co.makeapp.data.model;

import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * Created by nguni52 on 2016/10/03.
 */
@Data
@NoArgsConstructor
public class PaymentDetails {
    //    private Address address;
    private OurCreditCard creditCard;
    private String price;
    private String cvv2;
}
