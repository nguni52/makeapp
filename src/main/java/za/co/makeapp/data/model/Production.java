package za.co.makeapp.data.model;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import lombok.Data;
import org.joda.time.DateTime;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.format.annotation.DateTimeFormat;
import za.co.makeapp.config.LocalDateDeserializer;
import za.co.makeapp.config.LocalDateSerializer;

import java.util.List;

/**
 * Created by nguni52 on 2016/05/25.
 */
@Data
@Document
public class Production {
    @Id
    private String id;
    private String name;
    private Address officeAddress;
    private String tel;
    private String fax;
    private String execProducer;
    private String commEditor;
    private String creativeDirector;
    private String producer;
    private String director;
    private String continuity;
    private String lineProducer;
    private String prodManager;
    private String prodCoordinator;
    private List<Actor> actors;
    private List<Episode> episodes;
    @DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME)
    @JsonSerialize(using = LocalDateSerializer.class)
    @JsonDeserialize(using = LocalDateDeserializer.class)
    private DateTime creationDate;
}
