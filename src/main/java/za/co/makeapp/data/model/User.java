package za.co.makeapp.data.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

/**
 * Created by nguni52 on 16/04/25.
 */
@Data
@Document
@NoArgsConstructor
public class User {
    @Id
    private String username;
    private String firstName;
    private String lastName;
    @JsonProperty
    private String password;
    private String email;
    private String[] role = {"USER"};
    private String adminId;
    private String cellPhone;
    private String proPicId;
}
