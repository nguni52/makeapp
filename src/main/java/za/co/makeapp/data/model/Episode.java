package za.co.makeapp.data.model;


import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.joda.time.DateTime;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.format.annotation.DateTimeFormat;
import za.co.makeapp.config.LocalDateDeserializer;
import za.co.makeapp.config.LocalDateSerializer;

import java.util.Date;
import java.util.List;

/**
 * Created by nguni52 on 2016/06/01.
 */
@Document
@Data
@NoArgsConstructor
@AllArgsConstructor
public class Episode {
    @Id
    private String id;
    private String name;
    private String number;
    private String summary;
    private String productionId;
    //private Date shootingDate;
    private List<Scene> scenes;
    @DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME)
    @JsonSerialize(using = LocalDateSerializer.class)
    @JsonDeserialize(using = LocalDateDeserializer.class)
    private DateTime shootingDate;
    @DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME)
    @JsonSerialize(using = LocalDateSerializer.class)
    @JsonDeserialize(using = LocalDateDeserializer.class)
    private DateTime creationDate;
}
