package za.co.makeapp.data.model;

import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.mongodb.core.mapping.Document;

/**
 * Created by nguni52 on 2016/06/20.
 */
@Data
@NoArgsConstructor
@Document
public class Character extends Actor {
    private String characterFirstName;
    private String characterLastName;
    private String characterId;
    private String productionId;
    // @Todo - Make this a field for saving the actual image
    //private String actorPicture;
}
