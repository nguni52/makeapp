package za.co.makeapp.data.model;

import lombok.Data;

/**
 * Created by nguni52 on 2016/05/25.
 */
@Data
public class Address {
    private String street1;
    private String street2;
    private String suburb;
    private String city;
    private String code;
    private String province;
    private String country;
}
