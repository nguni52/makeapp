package za.co.makeapp.data.model;

import lombok.Data;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.annotation.Id;

import java.util.List;

/**
 * Created by mkmaposa on 2017/03/13.
 */

@Data
@Document
public class ProductionEmail {
    @Id
    private String id;
    private String productionId;
    private String email;
}
