package za.co.makeapp.data.model;

import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;

/**
 * Created by nguni52 on 2016/10/03.
 */
@Data
@RequiredArgsConstructor
@NoArgsConstructor
public class OurCreditCard {
    @NonNull
    private String number;
    @NonNull
    private String type;
    @NonNull
    private int expireMonth;
    @NonNull
    private int expireYear;
    private String cvv2;
    private String firstName;
    private String lastName;
    private String payerId;
}
