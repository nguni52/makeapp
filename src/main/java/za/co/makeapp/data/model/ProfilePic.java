package za.co.makeapp.data.model;

import com.mongodb.DBObject;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;

import java.io.InputStream;

/**
 * Created by nguni52 on 2/18/17.
 */

@Data
@NoArgsConstructor
@RequiredArgsConstructor
public class ProfilePic { //extends UploadImage {
    @NonNull
    private InputStream inputStream;
    @NonNull
    private String fileName;
    @NonNull
    private String fileContentType;
    @NonNull
    private String username;
    private DBObject metaData;
    private long length;
    private String id;
}