package za.co.makeapp.data.model;

import com.mongodb.DBObject;
import lombok.Data;
import lombok.NonNull;

import java.io.InputStream;

/**
 * Created by nguni52 on 2016/08/06.
 */
@Data
public class UploadImage {
    @NonNull
    private InputStream inputStream;
    @NonNull
    private String fileName;
    @NonNull
    private String fileContentType;
    private DBObject metaData;
    private long length;
    private String id;
}
