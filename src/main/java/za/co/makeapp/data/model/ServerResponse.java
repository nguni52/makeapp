package za.co.makeapp.data.model;

import lombok.Data;

/**
 * Created by nguni52 on 16/04/25.
 */
@Data
public class ServerResponse {
    private String message;
    private boolean status;
}
