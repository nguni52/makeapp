package za.co.makeapp.data.model;

import lombok.AllArgsConstructor;
import lombok.Data;

/**
 * Created by nguni52 on 2017/03/31.
 */
@Data
@AllArgsConstructor
public class ImageComment {
    private String imageId;
    private String comment;
}
