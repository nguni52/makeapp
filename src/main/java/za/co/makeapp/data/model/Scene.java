package za.co.makeapp.data.model;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.joda.time.DateTime;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.format.annotation.DateTimeFormat;
import za.co.makeapp.config.LocalDateDeserializer;
import za.co.makeapp.config.LocalDateSerializer;

import java.util.List;

/**
 * Created by nguni52 on 2016/06/01.
 */
@Document
@Data
@NoArgsConstructor
public class Scene {
    @Id
    private String id;
    private String number; //scene number
    // @ToDo - vovas - change the comments
    private String description; //actor's real last name
    // @ToDo - vovas - change the comments
    private String intOrExt; //character's first name in the production
    // @ToDo - vovas - change the comments
    private String dayOrNight; //character's last name in the production
    private String screenDay;
    private Boolean hasBeenShot=false;
    private String episodeId;
    private List<Character> characters;

    @DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME)
    @JsonSerialize(using = LocalDateSerializer.class)
    @JsonDeserialize(using = LocalDateDeserializer.class)
    private DateTime shootingDate;
    @DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME)
    @JsonSerialize(using = LocalDateSerializer.class)
    @JsonDeserialize(using = LocalDateDeserializer.class)
    private DateTime dateShot;
    @DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME)
    @JsonSerialize(using = LocalDateSerializer.class)
    @JsonDeserialize(using = LocalDateDeserializer.class)
    private DateTime creationDate;
}
