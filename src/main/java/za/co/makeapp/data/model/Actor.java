package za.co.makeapp.data.model;

/**
 * Created by Kenneth.Maposa on 2016/05/14.
 */

import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Data
@Document
@NoArgsConstructor
public class Actor {
    @Id
    private String id;
    private String firstName; //actor's real first name
    private String lastName; //actor's real last name
    private String gender;
}
