package za.co.makeapp.data.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

/**
 * Created by nguni52 on 2016/10/03.
 */
@Data
@Document(collection = "package")
@NoArgsConstructor
@AllArgsConstructor
public class ProductionPackage {
    @Id
    private String id;
    private String name;
    private String price;
    private int numberOfEpisodes;
    private int numberOfCharacters;
}
