package za.co.makeapp.data.builder;

import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.mongodb.core.mapping.Document;

import java.io.Serializable;

/**
 * Created by nguni52 on 16/04/25.
 */
@Data
@Document
@NoArgsConstructor
public class ActorBuilder implements Serializable {

}
