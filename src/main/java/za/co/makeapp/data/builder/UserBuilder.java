package za.co.makeapp.data.builder;

import za.co.makeapp.data.model.User;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by nguni52 on 16/04/27.
 */
public class UserBuilder {
    private String username;
    private String firstName;
    private String lastName;
    private String password;
    private String email;

    public UserBuilder(String username, String firstName, String lastName, String password, String email) {
        this.username = username;
        this.firstName = firstName;
        this.lastName = lastName;
        this.password = password;
        this.email = email;
    }

    public static User buildAUser(int i) {
        String username = "username" + i;
        String firstName = "first name" + i;
        String lastName = "last name " + i;
        String password = "password" + i;
        String email = "username" + i + "@makeapp.com";

        return new UserBuilder(username, firstName, lastName, password, email).build();
    }

    private User build() {

        User user = new User();
        user.setUsername(username);
        user.setFirstName(firstName);
        user.setLastName(lastName);
        user.setPassword(password);
        user.setEmail(email);

        return user;
    }

    public static List<User> buildUsers(int i) {
        List<User> users = new ArrayList<>();
        for (int j = 0; j < i; j++) {
            users.add(buildAUser(j));
        }
        return users;
    }
}
