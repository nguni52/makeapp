package za.co.makeapp.data.builder;

import za.co.makeapp.data.model.ProductionPackage;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by nguni52 on 2016/10/03.
 */
public class PackageBuilder {
    private String id;
    private String name;
    private String price;
    private int numberOfEpisodes;
    private int numberOfCharacters;

    public PackageBuilder(String id, String name, String price, int numberOfEpisodes, int numberOfCharacters) {
        this.id = id;
        this.name = name;
        this.price = price;
        this.numberOfEpisodes = numberOfEpisodes;
        this.numberOfCharacters = numberOfCharacters;
    }

    public static List<ProductionPackage> buildPackages(int i) {
        List<ProductionPackage> productionPackages = new ArrayList<>();
        for (int j = 1; j <= i; j++) {
            productionPackages.add(buildAPackage(j));
        }
        return productionPackages;
    }

    public static ProductionPackage buildAPackage(int j) {
        String id = "id" + j;
        String name = "name" + j;
        String price = "" + (100 * j);
        int numberOfEpisodes = j * 10;
        int numberOfCharacters = j * 20;

        return new PackageBuilder(id, name, price, numberOfEpisodes, numberOfCharacters).build();
    }

    private ProductionPackage build() {
        return new ProductionPackage(id, name, price, numberOfEpisodes, numberOfCharacters);
    }
}
