package za.co.makeapp.data.builder;

import lombok.Data;
import za.co.makeapp.data.model.OurCreditCard;

/**
 * Created by nguni52 on 2016/10/03.
 */
@Data
public class CreditCardBuilder {
    private String number;
    private String type;
    private int expireMonth;
    private int expireYear;
    private String cvv2;
    private String firstName;
    private String lastName;
    private String payerId;

    public CreditCardBuilder(String number, String type, int expireMonth, int expireYear, String cvv2, String firstName, String lastName, String payerId) {
        this.number = number;
        this.type = type;
        this.expireMonth = expireMonth;
        this.expireYear = expireYear;
        this.cvv2 = cvv2;
        this.firstName = firstName;
        this.lastName = lastName;
        this.payerId = payerId;
    }

    public static OurCreditCard buildACreditCard(int i) {
        String number = "466942424666077" + (i % 9);
        String type = (i % 2 == 0) ? "visa" : "mastercard";
        int expireMonth = i % 13;
        int expireYear = 2010 + i;
        String cvv2 = "123";
        String firstName = "John";
        String lastName = "Doe";
        String payerId = "nguni52";

        return new CreditCardBuilder(number, type, expireMonth, expireYear, cvv2, firstName, lastName, payerId).build();
    }

    private OurCreditCard build() {
        OurCreditCard creditCard = new OurCreditCard(number, type, expireMonth, expireYear);
        creditCard.setCvv2(this.cvv2);
        creditCard.setFirstName(this.firstName);
        creditCard.setLastName(this.lastName);

        return creditCard;
    }
}
