package za.co.makeapp.data.builder;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import org.joda.time.DateTime;
import org.springframework.format.annotation.DateTimeFormat;
import za.co.makeapp.config.LocalDateDeserializer;
import za.co.makeapp.config.LocalDateSerializer;
import za.co.makeapp.data.model.Episode;
import za.co.makeapp.data.model.Scene;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by nguni52 on 11/12/16.
 */
public class EpisodeBuilder {
    private String id;
    private String name;
    private String number;
    private final String summary;
    private String productionId;
    @DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME)
    @JsonSerialize(using = LocalDateSerializer.class)
    @JsonDeserialize(using = LocalDateDeserializer.class)
    private DateTime shootingDate;
    private List<Scene> scenes;
    DateTime dateCreated = new DateTime();

    public EpisodeBuilder(String id, String name, String number, String summary, String productionId, DateTime shootingDate, List<Scene> scenes) {
        this.id = id;
        this.name = name;
        this.number = number;
        this.summary = summary;
        this.productionId = productionId;
        this.shootingDate = shootingDate;
        this.scenes = scenes;
    }

    public static Episode buildAnEpisode(int i) {
        String id = "" + i;
        String name = "name " + i;
        String number = "" + i;
        String summary = "summary " + i;
        String productionId = "production1";
        DateTime shootingDate = new DateTime();
        List<Scene> scenes = null;
        return new EpisodeBuilder(id, name, number, summary, productionId, shootingDate, scenes).build();
    }

    private Episode build() {
//        Episode episode = new Episode();
//        episode.setId(id);
//        episode.setName(name);
//        episode.setNumber(number);
//
        return new Episode(id, name, number, summary, productionId, scenes, shootingDate, dateCreated);
    }

    public static List<Episode> buildEpisodes(int numberOfEpisodes) {
        List<Episode> episodes = new ArrayList<>();
        for(int i=0;i<numberOfEpisodes;i++) {
            episodes.add(buildAnEpisode(i));
        }

        return episodes;
    }
}
