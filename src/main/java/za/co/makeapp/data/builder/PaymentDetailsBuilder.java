package za.co.makeapp.data.builder;

import za.co.makeapp.data.model.OurCreditCard;
import za.co.makeapp.data.model.PaymentDetails;

/**
 * Created by nguni52 on 2016/10/03.
 */
public class PaymentDetailsBuilder {
    //    private Address address;
    private OurCreditCard creditCard;
    private String price;
    private String cvv2;

    public PaymentDetailsBuilder(String price, String cvv2, OurCreditCard creditCard) {
        this.price = price;
        this.cvv2 = cvv2;
        this.creditCard = creditCard;
    }

    public static PaymentDetails buildAPaymentDetail(int i) {
        // Address address;
        OurCreditCard creditCard = CreditCardBuilder.buildACreditCard(i);
        String price = "" + (i * 100);
        String cvv2 = 21 + "" + i;


        return new PaymentDetailsBuilder(price, cvv2, creditCard).build();
    }

    private PaymentDetails build() {
        PaymentDetails paymentDetails = new PaymentDetails();
        paymentDetails.setPrice(this.price);
        paymentDetails.setCvv2(this.cvv2);
        paymentDetails.setCreditCard(this.creditCard);

        return paymentDetails;
    }
}
