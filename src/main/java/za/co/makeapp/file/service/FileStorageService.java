package za.co.makeapp.file.service;

import com.mongodb.DBObject;
import za.co.makeapp.data.model.ProfilePic;
import za.co.makeapp.data.model.UploadImage;

import java.io.InputStream;
import java.util.List;

/**
 * Created by nguni52 on 2016/08/06.
 */
public interface FileStorageService {
    String save(InputStream inputStream, String fileName, String fileContentType, DBObject metaData);
    List<UploadImage> findCharacterImagesForScene(String sceneId, String characterId);
    UploadImage findOne(String id);
    void delete(String imageId);
    ProfilePic getByUsernameAndId(String username, String proPicId) throws Exception;
}
