package za.co.makeapp.file.service;

import com.mongodb.DBObject;
import com.mongodb.gridfs.GridFSDBFile;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.gridfs.GridFsTemplate;
import org.springframework.stereotype.Service;
import za.co.makeapp.data.model.ProfilePic;
import za.co.makeapp.data.model.UploadImage;
import za.co.makeapp.data.model.User;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by nguni52 on 2016/08/06.
 */
@Service
public class FileStorageServer implements FileStorageService {
    @Autowired
    GridFsTemplate gridFsTemplate;
    private Log log = LogFactory.getLog(this.getClass().getName());

    @Override
    public String save(InputStream inputStream, String fileName, String fileContentType, DBObject metaData) {
        return this.gridFsTemplate.store(inputStream, fileName, fileContentType, metaData).getId().toString();
    }

    @Override
    public List<UploadImage> findCharacterImagesForScene(String sceneId, String characterId) {
        List<UploadImage> uploadFiles = new ArrayList<>();
        Query query = new Query();
        query.addCriteria(Criteria.where("metadata.sceneId").is(sceneId));
        query.addCriteria(Criteria.where("metadata.characterId").is(characterId));
        query.with(new Sort(Sort.Direction.DESC, "uploadDate"));

        List<GridFSDBFile> gridFSDBFiles = gridFsTemplate.find(query);

        for (GridFSDBFile gridFSDBFile : gridFSDBFiles) {
            UploadImage uploadFile = new UploadImage(gridFSDBFile.getInputStream(), gridFSDBFile.getFilename(),
                    gridFSDBFile.getContentType());
            uploadFile.setLength(gridFSDBFile.getLength());
            uploadFile.setId(gridFSDBFile.getId().toString());
            uploadFiles.add(uploadFile);
        }
        return uploadFiles;
    }

    @Override
    public UploadImage findOne(String id) {
        Query query = new Query();
        query.addCriteria(Criteria.where("_id").is(id));
        query.limit(1);
        GridFSDBFile gridFSDBFile =  gridFsTemplate.findOne(query);
        log.info(gridFSDBFile.toString());
        UploadImage uploadImage = new UploadImage(gridFSDBFile.getInputStream(), gridFSDBFile.getFilename(),
                gridFSDBFile.getContentType());
        uploadImage.setLength(gridFSDBFile.getLength());
        uploadImage.setId(gridFSDBFile.getId().toString());
        uploadImage.setMetaData(gridFSDBFile.getMetaData());

        return uploadImage;
    }

    @Override
    public void delete(String imageId) {
        Query query = new Query();
        query.addCriteria(Criteria.where("_id").is(imageId));
        gridFsTemplate.delete(query);
    }

    @Override
    public ProfilePic getByUsernameAndId(String username, String proPicId) throws Exception {
        Query query = new Query();
        query.addCriteria(Criteria.where("metadata.username").is(username));
        query.limit(1);
        query.with(new Sort(Sort.Direction.DESC, "uploadDate"));

        GridFSDBFile gridFSDBFile = gridFsTemplate.findOne(query);
        ProfilePic profilePic = new ProfilePic(gridFSDBFile.getInputStream(), gridFSDBFile.getFilename(),
                gridFSDBFile.getContentType(), gridFSDBFile.getMetaData().get("username").toString());
        profilePic.setLength(gridFSDBFile.getLength());

        return profilePic;
    }
}
