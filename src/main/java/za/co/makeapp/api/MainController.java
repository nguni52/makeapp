package za.co.makeapp.api;

/**
 * Created by nguni52 on 16/04/25.
 */
public class MainController {
    public static final String HOME = "/";
    public static final String API="api";
    public static final String USER="user";
    public static final String ACTOR="actor";
    public static final String LIST = "list";
    public static final String UPDATE = "update";
    public static final String DELETE = "delete";
    public static final String CREATE = "create";
    public static final String TITLE = "title";
    public static final String INDEX = "index";
    public static final String ADD = "add";
    public static final String EDIT = "edit";
    public static final String DETAILS = "details";
    public static final String ID = "id";
    public static final String ACTORNAME = "actorname";
    public static final String FIRSTNAME = "firstname";
    public static final String LASTNAME = "lastname";
    public static final String SAVE = "save";
    public static final String UPLOAD = "upload";
    public static final String IMAGE = "image";
    public static final String HOME_ID = HOME + "{" + ID + "}";
    public static final String HOME_LIST = HOME + LIST;
    public static final String HOME_UPDATE = HOME + UPDATE;
    public static final String HOME_ADD = HOME + ADD;
    public static final String EMAIL = "email";
    public static final String REPORT = "report";
}
