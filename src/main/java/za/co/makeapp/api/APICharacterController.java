package za.co.makeapp.api;

import org.apache.commons.io.IOUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import za.co.makeapp.character.service.CharacterService;
import za.co.makeapp.data.model.ImageComment;
import za.co.makeapp.data.model.ServerResponse;
import za.co.makeapp.data.model.UploadImage;
import za.co.makeapp.production.controller.CharacterController;
import za.co.makeapp.scene.controller.SceneController;

import javax.servlet.http.HttpServletResponse;
import java.util.List;
import java.util.Locale;


/**
 * Created by nguni52 on 2016/08/06.
 */
@RestController
@RequestMapping(value=APICharacterController.API_CHARACTER)
public class APICharacterController extends MainController {
    public static final String API_CHARACTER = API + HOME + CharacterController.CHARACTER;
    private static final String HOME_UPLOAD_SCENE_CHARACTER = HOME + UPLOAD + HOME + "{" + ID + "}" + HOME + "{" +
            SceneAPIController.CHARACTER_ID + "}";
    public static final String HOME_IMAGE_SCENE_ID = HOME + IMAGE + HOME + SceneController.SCENE + HOME +
            "{" + ID + "}" + HOME + "{" + SceneAPIController.CHARACTER_ID + "}";
    private static final String HOME_IMAGE_ID = HOME + IMAGE + HOME + "{" + ID + "}";
    private static final String COMMENT = "comment";
    private static final String HOME_IMAGE_COMMENT_ID = HOME + IMAGE + HOME + COMMENT + HOME + "{" + ID + "}";
    private Log log = LogFactory.getLog(this.getClass().getName());
    @Autowired
    private CharacterService characterService;
    @Autowired
    private MessageSource messageSource;

    @RequestMapping(method = RequestMethod.POST, value = HOME_UPLOAD_SCENE_CHARACTER)
    public ServerResponse uploadCharacterImageForScene(@PathVariable(ID) String sceneId,
                                                       @PathVariable(SceneAPIController.CHARACTER_ID) String characterId,
                                                       final @RequestParam("uploadImage") MultipartFile profilePic,
                                                       final @RequestParam("comment") String imageComment,
                                                       Locale locale) {
        ServerResponse serverResponse = new ServerResponse();
        String message, imageId = "";
        try {
            log.info("Saving character image for character with id: " + characterId);
            log.info("The image comment is: " + imageComment);
            UploadImage uploadImage = new UploadImage(profilePic.getInputStream(), profilePic.getName(),
                    profilePic.getContentType());
            characterService.uploadImage(uploadImage, sceneId, characterId, imageComment);
            message = messageSource.getMessage("character.image.upload.success", null, locale);
            serverResponse.setMessage(message);
            serverResponse.setStatus(true);
        } catch (Exception ex) {
            message = messageSource.getMessage("character.image.upload.failure", null, locale);
            serverResponse.setStatus(false);
            serverResponse.setMessage(message);
        }
        return serverResponse;
    }

    @RequestMapping(method = RequestMethod.GET, value = HOME_IMAGE_SCENE_ID)
    public List<String> getCharacterImageIds(@PathVariable(ID) String sceneId,
                                             @PathVariable(SceneAPIController.CHARACTER_ID) String characterId) {
        return characterService.findCharacterImagesForScene(sceneId, characterId);
    }

    @RequestMapping(method = RequestMethod.GET, value = HOME_IMAGE_ID)
    public void getImageById(@PathVariable(ID) String id, HttpServletResponse response) {
        log.info("IMage id is: " + id);

        UploadImage uploadImage = characterService.findCharacterImage(id);
        log.info(uploadImage.toString());
        try {
            byte[] data = IOUtils.toByteArray(uploadImage.getInputStream());
            response.setContentType(uploadImage.getFileContentType());
            response.setContentLength((int) uploadImage.getLength());
            response.getOutputStream().write(data);
            response.getOutputStream().flush();
        } catch (Exception ex) {
            response.setStatus(HttpStatus.NOT_FOUND.value());
        }
    }

    @RequestMapping(method = RequestMethod.GET, value = HOME_IMAGE_COMMENT_ID)
    public ImageComment getImageComment(@PathVariable(ID) String id) {
        return characterService.getImageCommentByImageId(id);
    }

    @RequestMapping(method = RequestMethod.DELETE, value = HOME_IMAGE_ID)
    public ServerResponse deleteCharacterImage(@PathVariable(ID) String imageId, Locale locale) {
        ServerResponse serverResponse = new ServerResponse();
        String message;
        try {
            characterService.deleteCharacterImage(imageId);
            message = messageSource.getMessage("character.image.delete.success", null, locale);
            serverResponse.setMessage(message);
            serverResponse.setStatus(true);
        } catch (Exception ex) {
            message = messageSource.getMessage("character.image.delete.failure", null, locale);
            serverResponse.setStatus(false);
            serverResponse.setMessage(message);
        }

        return serverResponse;
    }
}
