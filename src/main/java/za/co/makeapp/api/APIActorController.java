package za.co.makeapp.api;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.web.bind.annotation.*;
import za.co.makeapp.actor.service.ActorService;
import za.co.makeapp.data.model.Actor;
import za.co.makeapp.data.model.ServerResponse;

import java.util.Collection;

/**
 * Created by nguni52 on 16/04/25.
 */

@RestController
@RequestMapping(value= APIActorController.API_ACTOR)
public class APIActorController extends MainController {
    private Log log = LogFactory.getLog(this.getClass().getName());
    public static final String HOME_ACTORNAME = HOME + "{" + FIRSTNAME + "}" + HOME + "{" + LASTNAME + "}";
    public static final String HOME_DELETE_ID = HOME + DELETE + HOME + "{" + ID + "}";
    public static final String API_ACTOR = API + HOME + ACTOR;
    private static final String HOME_ADD_ID = HOME_ADD + HOME + "{" + ID + "}";

    @Autowired
    private ActorService actorService;
    @Autowired
    private MessageSource messageSource;


    @RequestMapping(method = RequestMethod.POST, value = HOME_ADD)
    public ServerResponse create(@RequestBody Actor actor) {
        String message;
        ServerResponse serverResponse = new ServerResponse();

        log.info("Actor :: " + actor.toString());
        try {
            actorService.create(actor);
            message = messageSource.getMessage("actor.create.success", null, null);
            serverResponse.setMessage(message);
            serverResponse.setStatus(true);
        } catch(Exception ex) {
            serverResponse.setMessage(ex.getMessage());
            serverResponse.setStatus(false);
        }

        return serverResponse;
    }

    @RequestMapping(method = RequestMethod.GET, value=HOME_ID)
    public Actor findById(@PathVariable(ID) String id) {
        return actorService.findOne(id);
    }

    @RequestMapping(method = RequestMethod.GET, value = HOME_ACTORNAME)
    public Actor getActor(@PathVariable(FIRSTNAME) String firstName,
                          @PathVariable(LASTNAME) String lastName) {
        log.info("Actor name is: " + firstName + ":: " + lastName);
        return actorService.findByActorName(firstName, lastName);
    }

    @RequestMapping(method = RequestMethod.GET, value = HOME_LIST)
    public Collection<Actor> getActors() {
        return actorService.findAll();
    }

    @RequestMapping(method = RequestMethod.PUT, value = HOME_ADD_ID)
    public ServerResponse updateActor(@RequestBody Actor actor,
                                      @PathVariable(ID) String id) {
        String message; // = StringUtils.EMPTY;
        ServerResponse serverResponse = new ServerResponse();

        log.info("Actor :: " + actor.toString());
        try {
            actor.setId(id);
            actorService.update(actor);
            message = messageSource.getMessage("actor.update.success", null, null);
            serverResponse.setMessage(message);
            serverResponse.setStatus(true);
        } catch(Exception ex) {
            //message = messageSource.getMessage("user.create.failure", null, null);
            serverResponse.setMessage(ex.getMessage());
            serverResponse.setStatus(false);
        }

        return serverResponse;
    }

    @RequestMapping(method = RequestMethod.DELETE, value = HOME_DELETE_ID)
    public ServerResponse deleteActor(@PathVariable(ID) String id) {
        String message; // = StringUtils.EMPTY;
        ServerResponse serverResponse = new ServerResponse();

        try {
            actorService.delete(id);
            message = messageSource.getMessage("actor.delete.success", null, null);
            serverResponse.setMessage(message);
            serverResponse.setStatus(true);
        } catch(Exception ex) {
            //message = messageSource.getMessage("user.create.failure", null, null);
            serverResponse.setMessage(ex.getMessage());
            serverResponse.setStatus(false);
        }

        return serverResponse;
    }
}
