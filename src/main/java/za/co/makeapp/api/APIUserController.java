package za.co.makeapp.api;

import org.apache.commons.io.IOUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import za.co.makeapp.data.model.ProfilePic;
import za.co.makeapp.data.model.ServerResponse;
import za.co.makeapp.data.model.User;
import za.co.makeapp.user.service.UserService;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.security.Principal;
import java.util.Collection;
import java.util.Locale;

/**
 * Created by nguni52 on 16/04/25.
 */
@RestController
@RequestMapping(value = APIUserController.API_USER)
public class APIUserController extends MainController {
    private static final String USERNAME = "username";
    private static final String LOGIN = "login";
    private static final String PROFILEPIC = "profilepic";
    public static final String HOME_USERNAME = HOME + "{" + USERNAME + "}";
    public static final String HOME_USER_EMAIL = MainController.HOME + "{" + USERNAME + ":.+}/";
    public static final String HOME_DELETE_USERNAME = HOME+DELETE+HOME + "{" + USERNAME + "}";
    public static final String HOME_LOGIN = HOME + LOGIN;
    public static final String HOME_UPLOAD_PROFILEPIC = HOME + UPLOAD + HOME + PROFILEPIC + HOME + "{"+ USERNAME + "}";
    public static final String HOME_UPLOAD_PROFILEPIC_EMAIL = HOME + UPLOAD + HOME + PROFILEPIC + HOME + "{"+ USERNAME + ":.+}/";


    private Log log = LogFactory.getLog(APIUserController.class);
    public static final String API_USER = API + HOME + USER;
    private static final String REGISTER = "register";
    public static final String HOME_REGISTER = HOME + REGISTER;
    private static final String CHANGE = "change";
    private static final String PW = "pw";
    private static final String HOME_PW_CHANGE = HOME + PW + HOME + CHANGE;
    private static final String CHECK = "check";
    private static final String HOME_PW_CHECK = HOME + PW + HOME + CHECK;

    @Autowired
    private UserService userService;
    @Autowired
    private MessageSource messageSource;


    /**
     * curl -i -X POST -H "Content-Type:application/json"
     * "http://52.1.124.222:9000/api/user/register" -d '{"username":"phakelam", "firstName":"Matiisetso",
     * "lastName":"Phakela", "password":"tiisetso", "email":"phakelam@icloud.com"}'
     *
     * // -H "Authorization: Bearer 089e0fcd1ce14c5350e26c10f775cf5a"
     * @param user
     * @return
     */
    @RequestMapping(method = RequestMethod.POST, value = HOME_REGISTER)
    public ServerResponse register(@RequestBody User user) {
        String message;
        ServerResponse serverResponse = new ServerResponse();
        try {
            userService.create(user);
            message = messageSource.getMessage("user.create.success", null, null);
            serverResponse.setMessage(message);
            serverResponse.setStatus(true);
        } catch(Exception ex) {
            serverResponse.setMessage(ex.getMessage());
            serverResponse.setStatus(false);
        }

        return serverResponse;
    }

    @RequestMapping(method = RequestMethod.GET, value = "/getcurrentuser")
    public String getCurrentUser(Principal principal) {
        return principal.getName();
    }

    @RequestMapping(method = RequestMethod.GET, value = {HOME_USERNAME, HOME_USER_EMAIL})
    public User getUser(@PathVariable(USERNAME) String username) {
        log.info("Username is: " + username);
        return userService.findByUsername(username);
    }

    @RequestMapping(method = RequestMethod.GET, value = HOME_LIST)
    public Collection<User> getUsers() {
        return userService.findAll();
    }

    //update
    @RequestMapping(method = RequestMethod.POST, value = HOME_UPDATE)
    public ServerResponse updateUser(@RequestBody User user) {
        String message; // = StringUtils.EMPTY;
        ServerResponse serverResponse = new ServerResponse();

        log.info("User :: " + user.toString());
        try {
            userService.update(user);
            message = messageSource.getMessage("user.update.success", null, null);
            serverResponse.setMessage(message);
            serverResponse.setStatus(true);
        } catch(Exception ex) {
            //message = messageSource.getMessage("user.create.failure", null, null);
            serverResponse.setMessage(ex.getMessage());
            serverResponse.setStatus(false);
        }

        return serverResponse;
    }

    @RequestMapping(method = RequestMethod.GET, value = HOME_DELETE_USERNAME)
    public ServerResponse deleteUser(@PathVariable(USERNAME) String username) {
        String message; // = StringUtils.EMPTY;
        ServerResponse serverResponse = new ServerResponse();

        try {
            userService.delete(username);
            message = messageSource.getMessage("user.delete.success", null, null);
            serverResponse.setMessage(message);
            serverResponse.setStatus(true);
        } catch(Exception ex) {
            //message = messageSource.getMessage("user.create.failure", null, null);
            serverResponse.setMessage(ex.getMessage());
            serverResponse.setStatus(false);
        }

        return serverResponse;
    }

    @RequestMapping(method = RequestMethod.POST, value = HOME_LOGIN)
    public ServerResponse loginUser(@RequestBody User user, Locale locale) {
        ServerResponse serverResponse = new ServerResponse();
        String message;

        if (user.getUsername().isEmpty() || user.getPassword().isEmpty()) {
            message = messageSource.getMessage("user.login.fieldsmissing", null, locale);
            serverResponse.setMessage(message);
            serverResponse.setStatus(false);

            return serverResponse;
        }
        try {
            User loggedInUser = userService.loginUser(user);
            if (loggedInUser != null) {
                message = messageSource.getMessage("user.login.success", null, locale);
                serverResponse.setMessage(message);
                serverResponse.setStatus(true);
            } else {
//                message = messageSource.getMessage("user.login.incorrect", null, locale);
                message = "Incorrect username/password combination";
                log.info("message:: " + message);
                serverResponse.setMessage(message);
                serverResponse.setStatus(false);
            }
        } catch (Exception ex) {
            log.info(ex);
            ex.printStackTrace();
            serverResponse.setMessage(ex.getMessage());
            serverResponse.setStatus(false);
        }

        return serverResponse;
    }

    @RequestMapping(method = RequestMethod.POST, value = HOME_PW_CHECK)
    public ServerResponse checkPassword(@RequestBody User user, Locale locale) {
        ServerResponse serverResponse = new ServerResponse();

        String message;
        try {
            boolean isCorrect = userService.checkPassword(user.getUsername(), user.getPassword());
            if(isCorrect) {
                message = messageSource.getMessage("user.pw.check.success", null, locale);
                serverResponse.setMessage(message);
                serverResponse.setStatus(true);
            } else {
                message = messageSource.getMessage("user.pw.check.failure", null, locale);
                serverResponse.setMessage(message);
                serverResponse.setStatus(false);
            }

        } catch(Exception ex) {
            serverResponse.setMessage(ex.getMessage());
            serverResponse.setStatus(false);
        }

        return serverResponse;
    }

    @RequestMapping(method = RequestMethod.POST, value = HOME_PW_CHANGE)
    public ServerResponse changePassword(@RequestBody User user, Locale locale) {
        ServerResponse serverResponse = new ServerResponse();
        String message;
        try {
            log.info("User is: " + user.getUsername() + ":: password --- " + user.getPassword());
            userService.changePassword(user.getUsername(), user.getPassword().trim());
            message = messageSource.getMessage("user.pw.change.success", null, locale);
            serverResponse.setMessage(message);
            serverResponse.setStatus(true);
        } catch(Exception ex) {
            serverResponse.setMessage(ex.getMessage());
            serverResponse.setStatus(false);
        }

        return serverResponse;
    }

    @RequestMapping(value = {HOME_UPLOAD_PROFILEPIC, HOME_UPLOAD_PROFILEPIC_EMAIL}, method = RequestMethod.POST)
    public ServerResponse updateUserProfilePic(final @PathVariable(USERNAME) String username,
                                               final @RequestParam("uploadImage") MultipartFile profilePic,
                                               final Locale locale) {
        ServerResponse serverResponse = new ServerResponse();
        String message;
        try {
            log.info("Saving profile pic for user: " + username);
            ProfilePic proPicFile = new ProfilePic(profilePic.getInputStream(), profilePic.getName(),
                    profilePic.getContentType(), username);
            userService.uploadProfilePic(username, proPicFile);
            message = messageSource.getMessage("user.account.profilepic.update.success", null, locale);
            serverResponse.setMessage(message);
            serverResponse.setStatus(true);
        } catch (Exception ex) {
            message = messageSource.getMessage("user.account.profilepic.update.failure", null, locale);
            serverResponse.setStatus(false);
            serverResponse.setMessage(message);

        }

        return serverResponse;
    }

    @RequestMapping(value = { HOME_UPLOAD_PROFILEPIC, HOME_UPLOAD_PROFILEPIC_EMAIL}, method = RequestMethod.GET, produces = MediaType.IMAGE_JPEG_VALUE)
    public void getStoreProfilePicById(@PathVariable(USERNAME) String username, HttpServletResponse response)
            throws IOException {
        try {
            ProfilePic proPic = userService.getProPicByUsername(username);

            try {
                byte[] data = IOUtils.toByteArray(proPic.getInputStream());
                response.setContentType(proPic.getFileContentType());
                response.setContentLength((int) proPic.getLength());
                response.getOutputStream().write(data);
                response.getOutputStream().flush();
            } catch (Exception ex) {
                response.setStatus(HttpStatus.NOT_FOUND.value());
            }
        } catch(Exception ex) {
            response.setStatus(HttpStatus.NOT_FOUND.value());
        }


    }


}
