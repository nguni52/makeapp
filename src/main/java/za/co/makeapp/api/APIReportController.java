package za.co.makeapp.api;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import za.co.makeapp.data.model.Scene;
import za.co.makeapp.reports.controller.ReportController;
import za.co.makeapp.scene.service.SceneService;

import java.util.List;

import static za.co.makeapp.scene.controller.SceneController.SCENE;

/**
 * Created by nguni52 on 2017/06/29.
 */
@RestController
@RequestMapping(value = APIReportController.API_REPORT)
public class APIReportController extends  MainController {
    public static final String API_REPORT = API + HOME + ReportController.REPORT;
    public static final String HOME_SCENE_LIST = HOME + SCENE + HOME + LIST;

    @Autowired
    private SceneService sceneService;

    @RequestMapping(method = RequestMethod.GET, value = HOME_SCENE_LIST)
    public List<Scene> getScenes() {

        return sceneService.getScenesByDateShot();
    }
}
