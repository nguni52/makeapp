package za.co.makeapp.api;

import com.paypal.api.openidconnect.Tokeninfo;
import com.paypal.api.payments.CreditCard;
import com.paypal.api.payments.Payment;
import com.paypal.base.rest.APIContext;
import com.paypal.base.rest.PayPalRESTException;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.MessageSource;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import za.co.makeapp.data.model.PaymentDetails;
import za.co.makeapp.data.model.ProductionPackage;
import za.co.makeapp.data.model.ServerResponse;
import za.co.makeapp.production.service.PackageService;

import java.util.List;
import java.util.Locale;

/**
 * Created by nguni52 on 2016/10/03.
 */
@RestController
@RequestMapping(APIPackageController.API_PACKAGE)
public class APIPackageController {
    private static final String PACKAGE = "package";
    private static final String TOKEN = "token";
    private static final String ADDRESS = "address";
    private static final String CCDATA = "ccdata";
    private static final String PAYMENT = "payment";
    public static final String API_PACKAGE = MainController.API + MainController.HOME + PACKAGE;
    private static final String HOME_TOKEN = MainController.HOME + TOKEN;
    public static final String HOME_PAYMENT = MainController.HOME + PAYMENT;

    @Value("${payment.paypal.client.id}")
    private static String clientID;
    @Value("${payment.paypal.client.secret}")
    private static String clientSecret;

    @Autowired
    private PackageService packageService;
    private Log log = LogFactory.getLog(this.getClass().getName());
    @Autowired
    private MessageSource messageSource;

    @RequestMapping(method = RequestMethod.GET, value = MainController.HOME_LIST)
    public List<ProductionPackage> getPackages() {
        return packageService.getPackages();
    }

    @RequestMapping(method = RequestMethod.GET, value = HOME_TOKEN)
    public String getToken() throws PayPalRESTException {
        // Initialize apiContext with proper credentials and environment.
        APIContext context = new APIContext(clientID, clientSecret, "sandbox");


        // Create a Credit Card
        CreditCard card = new CreditCard()
                .setType("visa")
                .setNumber("4417119669820331")
                .setExpireMonth(11)
                .setExpireYear(2019)
                .setCvv2(012)
                .setFirstName("Joe")
                .setLastName("Shopper");

        // Replace the code with the code value returned from the redirect on previous step.
        String code = "hello";
        Tokeninfo info = Tokeninfo.createFromAuthorizationCode(context, code);
        String accessToken = info.getAccessToken();
//        String refreshToken = info.getRefreshToken();


        return accessToken;
    }

    @RequestMapping(method = RequestMethod.POST, value = HOME_PAYMENT, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ServerResponse processPayment(@RequestBody PaymentDetails paymentDetails, Locale locale) {
        ServerResponse serverResponse = new ServerResponse();
        String message;
        try {
            Payment createdPayment = packageService.processPayment(paymentDetails);
            log.info("Created payment invoice number: " + createdPayment.getTransactions().get(0).getInvoiceNumber());

            message = messageSource.getMessage("paypal.payment.success", null, locale);
            serverResponse.setMessage(message);
            serverResponse.setStatus(true);
        } catch (Exception e) {
            message = messageSource.getMessage("paypal.payment.failure", null, locale);
            serverResponse.setMessage(message);
            serverResponse.setStatus(false);
            log.info(e);
        }

        return serverResponse;
    }
}
