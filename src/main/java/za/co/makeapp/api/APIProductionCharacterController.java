package za.co.makeapp.api;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.web.bind.annotation.*;
import za.co.makeapp.actor.service.ActorService;
import za.co.makeapp.data.model.Actor;
import za.co.makeapp.data.model.Character;
import za.co.makeapp.data.model.ServerResponse;
import za.co.makeapp.production.controller.CharacterController;
import za.co.makeapp.production.controller.ProductionController;
import za.co.makeapp.character.service.CharacterService;

import java.util.Collection;

import static za.co.makeapp.production.controller.ProductionController.PRODID;
import static za.co.makeapp.production.controller.ProductionController.PRODUCTION;

/**
 * Created by nguni52 on 2016/06/20.
 */
@RestController
@RequestMapping(value = APIProductionCharacterController.API_CHARACTER)
public class APIProductionCharacterController extends MainController {
    private Log log = LogFactory.getLog(this.getClass().getName());
    public static final String HOME_ACTORNAME = HOME + "{" + FIRSTNAME + "}" + HOME + "{" + LASTNAME + "}";
    public static final String HOME_DELETE_ID = HOME + DELETE + HOME + "{" + ID + "}";
    public static final String API_CHARACTER = API + HOME + PRODUCTION + HOME + "{" + ProductionController.PRODID + "}" + HOME + CharacterController.CHARACTER;
    private static final String HOME_ADD_ID = HOME_ADD + HOME + "{" + ID + "}";

    @Autowired
    private CharacterService characterService;
    @Autowired
    private MessageSource messageSource;
    @Autowired
    private ActorService actorService;


    @RequestMapping(method = RequestMethod.POST, value = HOME_ADD)
    public ServerResponse create(@PathVariable(PRODID) String prodId,
                                 @RequestBody Character character) {
        String message;
        ServerResponse serverResponse = new ServerResponse();

        log.info("Character :: " + character.toString());
        try {
            Actor actor = actorService.findOne(character.getId());
            character.setFirstName(actor.getFirstName());
            character.setLastName(actor.getLastName());
            character.setGender(actor.getGender());
            character.setProductionId(prodId);
            characterService.create(character);
            message = messageSource.getMessage("character.create.success", null, null);
            serverResponse.setMessage(message);
            serverResponse.setStatus(true);
        } catch (Exception ex) {
            message = messageSource.getMessage("character.create.failure", null, null);
            serverResponse.setMessage(message);
            serverResponse.setStatus(false);
        }

        return serverResponse;
    }

    @RequestMapping(method = RequestMethod.GET, value = HOME_ID)
    public Character findById(@PathVariable(PRODID) String prodId,
                              @PathVariable(ID) String id) {
        return characterService.findOne(id);
    }

    @RequestMapping(method = RequestMethod.GET, value = HOME_LIST)
    public Collection<Character> getCharacters(@PathVariable(PRODID) String prodId) {
        return characterService.findAllByProdId(prodId);
    }

    @RequestMapping(method = RequestMethod.PUT, value = HOME_ADD_ID)
    public ServerResponse updateCharacter(@PathVariable(PRODID) String prodId,
                                          @RequestBody Character character,
                                          @PathVariable(ID) String id) {
        String message;
        ServerResponse serverResponse = new ServerResponse();

        log.info("Character :: " + character.toString());
        try {
            character.setId(id);
            characterService.update(character);
            message = messageSource.getMessage("character.update.success", null, null);
            serverResponse.setMessage(message);
            serverResponse.setStatus(true);
        } catch (Exception ex) {
            message = messageSource.getMessage("character.update.failure", null, null);
            serverResponse.setMessage(message);
            serverResponse.setStatus(false);
        }

        return serverResponse;
    }

    @RequestMapping(method = RequestMethod.DELETE, value = HOME_DELETE_ID)
    public ServerResponse deleteCharacter(@PathVariable(PRODID) String prodId,
                                          @PathVariable(ID) String id) {
        String message;
        ServerResponse serverResponse = new ServerResponse();

        try {
            characterService.delete(id);
            message = messageSource.getMessage("character.delete.success", null, null);
            serverResponse.setMessage(message);
            serverResponse.setStatus(true);
        } catch (Exception ex) {
            serverResponse.setMessage(ex.getMessage());
            serverResponse.setStatus(false);
        }

        return serverResponse;
    }
}
