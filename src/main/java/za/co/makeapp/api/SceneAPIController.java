package za.co.makeapp.api;

/**
 * Created by mkmaposa on 2016/06/02.
 */

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.web.bind.annotation.*;
import za.co.makeapp.data.model.Character;
import za.co.makeapp.data.model.Scene;
import za.co.makeapp.data.model.ServerResponse;
import za.co.makeapp.episode.controller.EpisodeController;
import za.co.makeapp.scene.controller.SceneController;
import za.co.makeapp.scene.service.SceneService;

import java.util.Arrays;
import java.util.Collection;
import java.util.List;

import static za.co.makeapp.production.controller.CharacterController.CHARACTER;
import static za.co.makeapp.scene.controller.SceneController.SCENE;

@RestController
@RequestMapping(value = SceneAPIController.API_SCENE)
public class SceneAPIController extends MainController{
    public static final String API_SCENE = API + HOME + SCENE;
    private static final String HOME_ID = HOME + "{" + ID + "}";
    private static final String HOME_DELETE_ID = HOME + DELETE + HOME + "{" + ID + "}";
    private static final String HOME_ADD_ID = HOME_ADD + HOME + "{" + ID + "}";
    private static final String EPISODE_HOME_LIST = HOME + "{" + EpisodeController.EPISODEID + "}" + HOME_LIST;
    private static final String HOME_ID_CHARACTER = HOME_ID + HOME + CHARACTER;
    public static final String HOME_ID_CHARACTER_SAVE = HOME_ID_CHARACTER + HOME + SAVE;
    public static final String CHARACTER_ID = "characterid";
    private static final String HOME_ID_CHARACTER_SCENE_ID = HOME_ID_CHARACTER + HOME + "{" + CHARACTER_ID + "}";
    private static final String HOME_ID_SCENE_REPORT =  HOME_ID + HOME + REPORT;

    private Log log = LogFactory.getLog(this.getClass().getName());

    @Autowired
    private SceneService sceneService;
    @Autowired
    private MessageSource messageSource;

    @RequestMapping(method = RequestMethod.POST, value = HOME_ADD)
    public ServerResponse addScene(@RequestBody Scene scene) {
        String message;
        ServerResponse serverResponse = new ServerResponse();

        log.info("Scene :: " + scene.toString());
        try {
            sceneService.create(scene);
            message = messageSource.getMessage("scene.create.success", null, null);
            serverResponse.setMessage(message);
            serverResponse.setStatus(true);
        } catch (Exception ex) {
            serverResponse.setMessage(ex.getMessage());
            serverResponse.setStatus(false);
        }

        return serverResponse;
    }

    @RequestMapping(method = RequestMethod.PUT, value = HOME_ADD_ID)
    public ServerResponse editScene(@RequestBody Scene scene,
                                    @PathVariable(ID) String id) {
        String message;
        ServerResponse serverResponse = new ServerResponse();

        log.info("Scene :: " + scene.toString());
        try {
            scene.setId(id);
            sceneService.update(scene);
            message = messageSource.getMessage("scene.update.success", null, null);
            serverResponse.setMessage(message);
            serverResponse.setStatus(true);
        } catch (Exception ex) {
            serverResponse.setMessage(ex.getMessage());
            serverResponse.setStatus(false);
        }

        return serverResponse;
    }

    @RequestMapping(method = RequestMethod.GET, value = EPISODE_HOME_LIST)
    public Collection<Scene> getScenes(@PathVariable(EpisodeController.EPISODEID) String episodeId) {
        return sceneService.findAll(episodeId);
    }

    @RequestMapping(method = RequestMethod.GET, value = HOME_ID)
    public Scene getAScene(@PathVariable(ID) String id) {
        return sceneService.findOne(id);
    }

    @RequestMapping(method = RequestMethod.DELETE, value = HOME_DELETE_ID)
    public ServerResponse deleteScene(@PathVariable(ID) String id) {
        String message;
        ServerResponse serverResponse = new ServerResponse();

        try {
            sceneService.delete(id);
            message = messageSource.getMessage("scene.delete.success", null, null);
            serverResponse.setMessage(message);
            serverResponse.setStatus(true);
        } catch (Exception ex) {
            serverResponse.setMessage(ex.getMessage());
            serverResponse.setStatus(false);
        }

        return serverResponse;
    }

    @RequestMapping(method = RequestMethod.GET, value = HOME_ID_CHARACTER)
    public List<Character> getSceneCharacters(@PathVariable(ID) String id) {
        return sceneService.findOne(id).getCharacters();
    }

    @RequestMapping(method = RequestMethod.POST, value = HOME_ID_CHARACTER_SAVE)
    public ServerResponse saveSceneCharacters(@PathVariable(ID) String sceneId,
                                              @RequestParam("characterIds") String[] characterIds) {
        ServerResponse serverResponse = new ServerResponse();
        String message;
        try {
            log.info("Scene id: " + sceneId);
            log.info("Character Ids" + Arrays.toString(characterIds));
            sceneService.saveCharacters(sceneId, characterIds);
            message = messageSource.getMessage("scene.save.characters.success", null, null);
            serverResponse.setMessage(message);
            serverResponse.setStatus(true);
        } catch (Exception ex) {
            ex.printStackTrace();
            serverResponse.setMessage(ex.getMessage());
            serverResponse.setStatus(false);
        }

        return serverResponse;
    }

    @RequestMapping(method = RequestMethod.DELETE, value = HOME_ID_CHARACTER_SCENE_ID)
    public ServerResponse deleteCharacterFromScene(@PathVariable(ID) String sceneId,
                                                   @PathVariable(CHARACTER_ID) String characterId) {
        ServerResponse serverResponse = new ServerResponse();
        String message;
        try {
            log.info("Character Id" + characterId);
            sceneService.deleteCharacterFromScene(sceneId, characterId);
            message = messageSource.getMessage("scene.delete.characters.success", null, null);
            serverResponse.setMessage(message);
            serverResponse.setStatus(true);
        } catch (Exception ex) {
            ex.printStackTrace();
            serverResponse.setMessage(ex.getMessage());
            serverResponse.setStatus(false);
        }

        return serverResponse;
    }

    @RequestMapping(method = RequestMethod.POST, value = HOME_ID_SCENE_REPORT)
    public ServerResponse addSceneToReport(@PathVariable(ID) String sceneId){
        ServerResponse serverResponse = new ServerResponse();
        String message;
        try{
            sceneService.addSceneToReport(sceneId);
            message = messageSource.getMessage("scene.add.report.success", null, null);
            serverResponse.setMessage(message);
            serverResponse.setStatus(true);
            log.info("Scene added to report : "+sceneId);
        }
        catch (Exception ex) {
            ex.printStackTrace();
            message = messageSource.getMessage("scene.add.report.failure", null, null);
            serverResponse.setMessage(ex.getMessage());
            serverResponse.setStatus(false);
        }
        return serverResponse;
    }
}
