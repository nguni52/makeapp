package za.co.makeapp.api;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.web.bind.annotation.*;
import za.co.makeapp.data.model.Production;
import za.co.makeapp.data.model.ProductionEmail;
import za.co.makeapp.data.model.ServerResponse;
import za.co.makeapp.production.controller.ProductionController;
import za.co.makeapp.production.service.ProductionService;
import za.co.makeapp.scene.controller.SceneController;

import java.util.List;

import static za.co.makeapp.production.controller.ProductionController.PRODID;

/**
 * Created by nguni52 on 2016/05/25.
 */
@RestController
@RequestMapping(value = APIProductionController.API_PRODUCTION)
public class APIProductionController extends MainController{
    public static final String API_PRODUCTION = API + HOME + ProductionController.PRODUCTION;
    private static final String HOME_ID = HOME + "{" + ID + "}";
    private static final String HOME_DELETE_ID = HOME + DELETE + HOME + "{" + ID + "}";
    private static final String HOME_ADD_ID = HOME_ADD + HOME + "{" + ID +  "}";
    public static final String HOME_SCENE_ID = HOME + SceneController.SCENE + HOME + "{" + ID + "}";
    private static final String HOME_ADD_EMAIL = HOME_ADD + HOME +EMAIL;
    private static final String HOME_LIST_EMAIL = HOME_LIST + HOME + "{" + EMAIL + ":.+}";
    private static final String PRODUCTION_HOME_EMAIL = HOME + "{" + PRODID + "}" + HOME + EMAIL;
    private static final String  HOME_DELETE_EMAIL_ID = HOME + DELETE + HOME + EMAIL + HOME +"{" + ID + "}";
    private static final String HOME_ADD_EMAIL_ID = HOME_ADD + HOME +EMAIL + HOME + "{" + ID + "}";

    private Log log = LogFactory.getLog(this.getClass().getName());
    @Autowired
    private ProductionService productionService;
    @Autowired
    private MessageSource messageSource;


    /**
     * This method is used to add production
     *
     * curl -i -X POST -H "Content-Type:application/json"
     * "http://localhost:8081/api/production/add" -d '{"name":"greed & desire"}'
     *
     * @param production
     * @return
     */
    @RequestMapping(method = RequestMethod.POST, value = HOME_ADD)
    public ServerResponse addProduction(@RequestBody Production production) {
        String message;
        ServerResponse serverResponse = new ServerResponse();

        log.info("Production :: " + production.toString());
        try {
            productionService.create(production);
            message = messageSource.getMessage("production.create.success", null, null);
            serverResponse.setMessage(message);
            serverResponse.setStatus(true);
        } catch(Exception ex) {
            serverResponse.setMessage(ex.getMessage());
            serverResponse.setStatus(false);
        }

        return serverResponse;
    }

    /**
     * This method is used to edit production
     *
     * curl -i -X PUT -H "Content-Type:application/json"
     * "http://localhost:8081/api/production/add/{id}" -d '{"name":"greed & desire"}'
     *
     * @param production
     * @return
     */
    @RequestMapping(method = RequestMethod.PUT, value = HOME_ADD_ID)
    public ServerResponse editProduction(@RequestBody Production production,
                                         @PathVariable(ID) String id) {
        String message;
        ServerResponse serverResponse = new ServerResponse();

        log.info("Production :: " + production.toString());
        try {
            production.setId(id);
            productionService.update(production);
            message = messageSource.getMessage("production.update.success", null, null);
            serverResponse.setMessage(message);
            serverResponse.setStatus(true);
        } catch(Exception ex) {
            serverResponse.setMessage(ex.getMessage());
            serverResponse.setStatus(false);
        }

        return serverResponse;
    }

    @RequestMapping(method = RequestMethod.GET, value = HOME_LIST)
    public List<Production> getProductions() {
        return productionService.findAll();
    }

    @RequestMapping(method = RequestMethod.GET, value = HOME_ID)
    public Production getAProduction(@PathVariable(ID)String id) {
        return productionService.findOne(id);
    }

    @RequestMapping(method = RequestMethod.DELETE, value = HOME_DELETE_ID)
    public ServerResponse deleteProduction(@PathVariable(ID) String id) {
        String message;
        ServerResponse serverResponse = new ServerResponse();

        try {
            productionService.delete(id);
            message = messageSource.getMessage("production.delete.success", null, null);
            serverResponse.setMessage(message);
            serverResponse.setStatus(true);
        } catch(Exception ex) {
            serverResponse.setMessage(ex.getMessage());
            serverResponse.setStatus(false);
        }

        return serverResponse;
    }

    @RequestMapping(method = RequestMethod.GET, value = HOME_SCENE_ID)
    public Production getProductionBySceneId(@PathVariable(ID) String sceneId) {
        return productionService.findProductionBySceneId(sceneId);
    }

    @RequestMapping(method = RequestMethod.POST, value = HOME_ADD_EMAIL)
    public ServerResponse addProductionEmail(@RequestBody ProductionEmail productionEmail) {
        String message;
        ServerResponse serverResponse = new ServerResponse();

        log.info("ProductionEmail :: " + productionEmail.toString());
        try {
            productionService.createProductionEmail(productionEmail);
            message = messageSource.getMessage("email.create.success", null, null);
            serverResponse.setMessage(message);
            serverResponse.setStatus(true);
        } catch(Exception ex) {
            serverResponse.setMessage(ex.getMessage());
            serverResponse.setStatus(false);
        }

        return serverResponse;
    }

    @RequestMapping(method = RequestMethod.GET, value = HOME_LIST_EMAIL)
    public List<Production> findUserProductions(@PathVariable(EMAIL) String myEmail) {
        List<ProductionEmail> productionEmails = productionService.findProductionsByEmail(myEmail);
        List<Production> userProductions = productionService.findUserProductions(productionEmails);
        return userProductions;
    }

    @RequestMapping(method = RequestMethod.GET, value = PRODUCTION_HOME_EMAIL)
    public List<ProductionEmail> getProductionEmails(@PathVariable(PRODID) String prodid) {
        return productionService.findEmailsForProduction(prodid);
    }

    @RequestMapping(method = RequestMethod.DELETE, value = HOME_DELETE_EMAIL_ID)
    public ServerResponse deleteUserEmail(@PathVariable(ID) String id) {
        String message;
        ServerResponse serverResponse = new ServerResponse();

        try {
            productionService.deleteUserEmail(id);
            message = messageSource.getMessage("email.delete.success", null, null);
            serverResponse.setMessage(message);
            serverResponse.setStatus(true);
        } catch(Exception ex) {
            serverResponse.setMessage(ex.getMessage());
            serverResponse.setStatus(false);
        }

        return serverResponse;
    }

    @RequestMapping(method = RequestMethod.PUT, value = HOME_ADD_EMAIL_ID)
    public ServerResponse editProductionEmail(@RequestBody ProductionEmail productionEmail,
                                         @PathVariable(ID) String id) {
        String message;
        ServerResponse serverResponse = new ServerResponse();

        log.info("ProductionEmail :: " + productionEmail.toString());
        try {
            productionEmail.setId(id);
            productionService.editProductionEmail(productionEmail);
            message = messageSource.getMessage("email.update.success", null, null);
            serverResponse.setMessage(message);
            serverResponse.setStatus(true);
        } catch(Exception ex) {
            serverResponse.setMessage(ex.getMessage());
            serverResponse.setStatus(false);
        }

        return serverResponse;
    }
}
