package za.co.makeapp.api;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.web.bind.annotation.*;
import za.co.makeapp.data.model.Episode;
import za.co.makeapp.data.model.ServerResponse;
import za.co.makeapp.episode.controller.EpisodeController;
import za.co.makeapp.episode.service.EpisodeService;

import java.util.List;

import static za.co.makeapp.production.controller.ProductionController.PRODID;

/**
 * Created by nguni52 on 2016/06/01.
 */
@RestController
@RequestMapping(value = EpisodeAPIController.API_EPISODE)
public class EpisodeAPIController extends MainController {

    public static final String API_EPISODE = API + HOME + EpisodeController.EPISODE;
    private static final String HOME_ID = HOME + "{" + ID + "}";
    private static final String HOME_DELETE_ID = HOME + DELETE + HOME + "{" + ID + "}";
    private static final String HOME_ADD_ID = HOME_ADD + HOME + "{" + ID + "}";
    private static final String PRODUCTION_HOME_LIST = HOME + "{" + PRODID + "}" + HOME + LIST;
    private Log log = LogFactory.getLog(this.getClass().getName());
    @Autowired
    private EpisodeService episodeService;
    @Autowired
    private MessageSource messageSource;


    /**
     * This method is used to add episode
     * <p>
     * curl -i -X POST -H "Content-Type:application/json"
     * "http://localhost:8081/api/episode/add" -d '{"name":"greed & desire"}'
     *
     * @param episode
     * @return
     */
    @RequestMapping(method = RequestMethod.POST, value = HOME_ADD)
    public ServerResponse addEpisode(@RequestBody Episode episode) {
        String message;
        ServerResponse serverResponse = new ServerResponse();

        log.info("Episode :: " + episode.toString());
        try {
            episodeService.create(episode);
            message = messageSource.getMessage("episode.create.success", null, null);
            serverResponse.setMessage(message);
            serverResponse.setStatus(true);
        } catch (Exception ex) {
            serverResponse.setMessage(ex.getMessage());
            serverResponse.setStatus(false);
        }

        return serverResponse;
    }

    /**
     * This method is used to edit episode
     * <p>
     * curl -i -X PUT -H "Content-Type:application/json"
     * "http://localhost:8081/api/episode/add/{id}" -d '{"name":"greed & desire"}'
     *
     * @param episode
     * @return
     */
    @RequestMapping(method = RequestMethod.PUT, value = HOME_ADD_ID)
    public ServerResponse editEpisode(@RequestBody Episode episode,
                                      @PathVariable(ID) String id) {
        String message;
        ServerResponse serverResponse = new ServerResponse();

        log.info("Episode :: " + episode.toString());
        try {
            episode.setId(id);
            episodeService.update(episode);
            message = messageSource.getMessage("episode.update.success", null, null);
            serverResponse.setMessage(message);
            serverResponse.setStatus(true);
        } catch (Exception ex) {
            serverResponse.setMessage(ex.getMessage());
            serverResponse.setStatus(false);
        }

        return serverResponse;
    }

    @RequestMapping(method = RequestMethod.GET, value = PRODUCTION_HOME_LIST)
    public List<Episode> getEpisodes(@PathVariable(PRODID) String prodid) {
        return episodeService.findAll(prodid);
    }

    @RequestMapping(method = RequestMethod.GET, value = HOME_ID)
    public Episode getAnEpisode(@PathVariable(ID) String id) {
        return episodeService.findOne(id);
    }

    @RequestMapping(method = RequestMethod.DELETE, value = HOME_DELETE_ID)
    public ServerResponse deleteEpisode(@PathVariable(ID) String id) {
        String message;
        ServerResponse serverResponse = new ServerResponse();

        try {
            episodeService.delete(id);
            message = messageSource.getMessage("episode.delete.success", null, null);
            serverResponse.setMessage(message);
            serverResponse.setStatus(true);
        } catch (Exception ex) {
            serverResponse.setMessage(ex.getMessage());
            serverResponse.setStatus(false);
        }

        return serverResponse;
    }
}
