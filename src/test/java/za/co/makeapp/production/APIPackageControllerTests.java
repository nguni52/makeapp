package za.co.makeapp.production;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.context.MessageSource;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockServletContext;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;
import za.co.makeapp.api.APIPackageController;
import za.co.makeapp.api.MainController;
import za.co.makeapp.data.builder.PaymentDetailsBuilder;
import za.co.makeapp.data.model.PaymentDetails;
import za.co.makeapp.data.repository.PackageRepository;
import za.co.makeapp.production.service.PackageService;

import java.io.IOException;

import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Created by nguni52 on 2016/10/03.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = MockServletContext.class)
@WebAppConfiguration
public class APIPackageControllerTests {
    @Autowired
    private WebApplicationContext webApplicationContext;
    private MockMvc mockMvc;
    private PaymentDetails paymentDetails;

    @Mock
    PackageService packageService;
    @Mock
    PackageRepository packageRepository;
    @Mock
    MessageSource messageSource;
    @InjectMocks
    APIPackageController packageController = new APIPackageController();

    private Log log = LogFactory.getLog(this.getClass().getName());

    @Before
    public void setUp() throws Exception {
        paymentDetails = PaymentDetailsBuilder.buildAPaymentDetail(1);
        MockitoAnnotations.initMocks(this);
        mockMvc = MockMvcBuilders.standaloneSetup(packageController).build();
    }

    @After
    public void tearDown() {
        packageRepository.deleteAll();
    }

    @Test
    public void testPaymentDetailsProcessing() {
        log.info("We about to test testPaymentDetailsProcessing in APIPackageControllerTests");

        try {
            mockMvc.perform(MockMvcRequestBuilders.post(MainController.HOME + APIPackageController.API_PACKAGE + APIPackageController.HOME_PAYMENT)
                    .contentType(MediaType.APPLICATION_JSON_UTF8)
                    .content(convertObjectToJsonBytes(paymentDetails)))
                    .andExpect(status().isOk())
                    .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8))
                    .andExpect(jsonPath("$.status").exists())
//                    .andExpect(jsonPath("$.message").exists())
                    .andDo(print());
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    public static byte[] convertObjectToJsonBytes(Object object) throws IOException {
        ObjectMapper mapper = new ObjectMapper();
        mapper.setSerializationInclusion(JsonInclude.Include.NON_NULL);
        return mapper.writeValueAsBytes(object);
    }
}
