package za.co.makeapp.production;

import com.paypal.api.payments.Payment;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import za.co.makeapp.TestConfig;
import za.co.makeapp.data.builder.PackageBuilder;
import za.co.makeapp.data.builder.PaymentDetailsBuilder;
import za.co.makeapp.data.model.PaymentDetails;
import za.co.makeapp.data.model.ProductionPackage;
import za.co.makeapp.data.repository.PackageRepository;
import za.co.makeapp.production.service.PackageService;

import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

/**
 * Created by nguni52 on 2016/10/03.
 */
public class PackageServiceTests extends TestConfig {
    @Autowired
    PackageService packageService;
    @Autowired
    private PackageRepository packageRepository;
    private Log log = LogFactory.getLog(this.getClass().getName());
    List<ProductionPackage> builtPackages;
    PaymentDetails paymentDetails;
    @Autowired
    private PackageService paymentService;

    @Before
    public void setUp() {
        builtPackages = PackageBuilder.buildPackages(10);
        packageRepository.save(builtPackages);

        paymentDetails = PaymentDetailsBuilder.buildAPaymentDetail(1);
    }

    public void tearDown() {
        packageRepository.deleteAll();
    }

    @Test
    public void testGetPackages() {
        log.info("RUNNING TEST GET PACKAGES\n\n\n\n\n");
        List<ProductionPackage> packages = packageService.getPackages();
        assertNotNull(packages);
        assertEquals(packages, builtPackages);
        assertEquals(packages.get(0).getName(), "name1");
    }

    @Test
    public void testPayment() {
        Payment payment = paymentService.processPayment(paymentDetails);
        log.info("INVOICE NUMBER IS: " + payment.getTransactions().get(0).getInvoiceNumber());
    }
}
