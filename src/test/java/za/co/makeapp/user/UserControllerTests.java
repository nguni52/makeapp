package za.co.makeapp.user;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockServletContext;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;
import za.co.makeapp.api.APIUserController;
import za.co.makeapp.api.MainController;
import za.co.makeapp.data.builder.UserBuilder;
import za.co.makeapp.data.model.User;
import za.co.makeapp.data.repository.UserRepository;
import za.co.makeapp.user.service.UserService;

import java.io.IOException;

import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Created by nguni52 on 16/04/27.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = MockServletContext.class)
@WebAppConfiguration
public class UserControllerTests {
    @Autowired
    private WebApplicationContext webApplicationContext;
    private MockMvc mockMvc;
    private User user;

    @Mock
    UserService userService;
    @Mock
    UserRepository userRepository;
    @InjectMocks
    APIUserController userController = new APIUserController();
    private Log log = LogFactory.getLog(this.getClass().getName());

    @Before
    public void setUp() throws Exception {
        user = UserBuilder.buildAUser(1);
        MockitoAnnotations.initMocks(this);
        mockMvc = MockMvcBuilders.standaloneSetup(userController).build();
    }

    @After
    public void tearDown() {
        userService.deleteAll();
    }

    @Test
    public void getIndex() throws Exception {
        Mockito.when(userService.findByUsername(user.getUsername())).thenReturn(user);

        mockMvc.perform(MockMvcRequestBuilders.get(MainController.HOME + APIUserController.API_USER + MainController.HOME + user.getUsername()))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(jsonPath("$.username").value(user.getUsername()))
                .andExpect(jsonPath("$.firstName").value(user.getFirstName()))
                .andExpect(jsonPath("$.lastName").value(user.getLastName()))
                .andExpect(jsonPath("$.email").value(user.getUsername() + "@makeapp.com"))
                .andDo(print());
    }

    @Test
    public void testSuccesfulUserLogin() throws Exception {
        User tempUser = new User();
        tempUser.setUsername(user.getUsername());
        tempUser.setPassword(user.getPassword());

        log.info("testSuccesfulUserLogin:::" + tempUser.toString() + "\n\n\n");

        mockMvc.perform(MockMvcRequestBuilders.post(MainController.HOME + APIUserController.API_USER + APIUserController.HOME_LOGIN)
                .contentType(MediaType.APPLICATION_JSON_UTF8)
                .content(convertObjectToJsonBytes(user)))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(jsonPath("$.status").value(false))
//                .andExpect(jsonPath("$.message").value(false))
                .andDo(print());
    }

    public static byte[] convertObjectToJsonBytes(Object object) throws IOException {
        ObjectMapper mapper = new ObjectMapper();
        mapper.setSerializationInclusion(JsonInclude.Include.NON_NULL);
        return mapper.writeValueAsBytes(object);
    }
}