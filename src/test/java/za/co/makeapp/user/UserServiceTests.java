package za.co.makeapp.user;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import za.co.makeapp.TestConfig;
import za.co.makeapp.data.builder.UserBuilder;
import za.co.makeapp.data.model.User;
import za.co.makeapp.data.repository.UserRepository;
import za.co.makeapp.user.service.UserService;

import java.util.Collection;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

/**
 * Created by nguni52 on 2016/06/27.
 */
public class UserServiceTests extends TestConfig {
    @Autowired
    UserService userService;
    User user;
    @Autowired
    private UserRepository userRepository;
    private Log log = LogFactory.getLog(this.getClass().getName());

    @Before
    public void setUp() {
        user = UserBuilder.buildAUser(12);
    }

    @After
    public void tearDown() {
        userRepository.deleteAll();
    }

    @Test
    public void testCreateUser() {
        log.info("RUNNING TEST CREATE USER\n\n\n\n");
        userService.create(user);
        User newUser = userService.findByUsername(user.getUsername());
        assertNotNull(newUser);
        assertEquals(user.getFirstName(), newUser.getFirstName());
        assertEquals(user, newUser);
        userRepository.delete(newUser);
    }

    @Test
    public void testGetUsers() {
        log.info("RUNNING TEST GET USERS\n\n\n\n");
        List<User> users = UserBuilder.buildUsers(10);
        assertNotNull(users);
        assertEquals(10, users.size());
        userRepository.save(users);
        Collection<User> tempUsers = userService.findAll();
        assertNotNull(tempUsers);
        log.info("User 1: " + tempUsers.iterator().next().toString() + "\n\n\n");
        assertEquals(tempUsers.size(), users.size());

    }

    @Test
    public void testUpdateUser() {
        log.info("RUNNING TEST update USER\n\n\n\n");
        user = UserBuilder.buildAUser(11);
        userService.create(user);
        String firstName = "New First Name";
        String surname = "New Surname";
        String email = "info@newemail.com";
        user.setFirstName(firstName);
        user.setLastName(surname);
        user.setEmail(email);

        userService.update(user);

        User newUser = userService.findByUsername(user.getUsername());
        assertEquals(firstName, newUser.getFirstName());
        assertEquals(surname, newUser.getLastName());
        assertEquals(email, newUser.getEmail());
        assertEquals(user, newUser);

        userRepository.delete(newUser);
    }

    //    @Test
    public void testDeleteUser() {

    }

    //    @Test
    public void testLoginUser() {

    }
}
