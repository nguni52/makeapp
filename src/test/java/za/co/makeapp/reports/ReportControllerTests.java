package za.co.makeapp.reports;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockServletContext;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;
import za.co.makeapp.api.APIReportController;
import za.co.makeapp.api.MainController;
import za.co.makeapp.reports.controller.ReportController;
import za.co.makeapp.scene.controller.SceneController;
import za.co.makeapp.scene.service.SceneService;

import java.io.IOException;

import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Created by nguni52 on 2017/06/29.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = MockServletContext.class)
@WebAppConfiguration
public class ReportControllerTests {
    @Autowired
    private WebApplicationContext webApplicationContext;
    private MockMvc mockMvc;
    private MockMvc apiMockMvc;
    @Mock
    SceneService sceneService;
    @InjectMocks
    APIReportController apiReportController = new APIReportController();

    ReportController reportController = new ReportController();
    private Log log = LogFactory.getLog(this.getClass().getName());

    @Before
    public void setUp() throws Exception {
        MockitoAnnotations.initMocks(this);
        mockMvc = MockMvcBuilders.standaloneSetup(reportController).build();
        apiMockMvc = MockMvcBuilders.standaloneSetup(apiReportController).build();
    }

    @Test
    public void getIndex() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.get(MainController.HOME + ReportController.REPORT))
                .andExpect(status().isOk())
                .andExpect(view().name(ReportController.REPORT_INDEX))
                .andDo(print());
    }

    @Test
    public void sceneIndexTest() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.get(MainController.HOME + ReportController.REPORT +
                MainController.HOME + SceneController.SCENE))
                .andExpect(status().isOk())
                .andExpect(view().name(ReportController.REPORT_SCENE_INDEX))
                .andDo(print());
    }

    @Test
    public void sceneAPIListTest() throws Exception {
        String sceneReportUrl = MainController.HOME + APIReportController.API_REPORT + APIReportController.HOME_SCENE_LIST;
        log.info(sceneReportUrl);
        apiMockMvc.perform(MockMvcRequestBuilders.get(sceneReportUrl))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8))
                .andDo(print());
    }

    public static byte[] convertObjectToJsonBytes(Object object) throws IOException {
        ObjectMapper mapper = new ObjectMapper();
        mapper.setSerializationInclusion(JsonInclude.Include.NON_NULL);
        return mapper.writeValueAsBytes(object);
    }
}
