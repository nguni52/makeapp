package za.co.makeapp.episode;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockServletContext;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;
import za.co.makeapp.api.EpisodeAPIController;
import za.co.makeapp.api.MainController;
import za.co.makeapp.data.builder.EpisodeBuilder;
import za.co.makeapp.data.model.Episode;
import za.co.makeapp.data.repository.EpisodeRepository;
import za.co.makeapp.episode.service.EpisodeService;

import java.io.IOException;

import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

/**
 * Created by nguni52 on 11/12/16.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = MockServletContext.class)
@WebAppConfiguration
public class EpisodeControllerTests {
    private Log log = LogFactory.getLog(this.getClass().getName());
    @Autowired
    private WebApplicationContext webApplicationContext;
    private MockMvc mockMvc;
    private Episode episode;

    @Mock
    EpisodeService episodeService;
    @Mock
    EpisodeRepository episodeRepository;
    @InjectMocks
    EpisodeAPIController episodeController = new EpisodeAPIController();

    @Before
    public void setUp() throws Exception {
        episode = EpisodeBuilder.buildAnEpisode(1);
        MockitoAnnotations.initMocks(this);
        mockMvc = MockMvcBuilders.standaloneSetup(episodeController).build();
    }

    @After
    public void tearDown() {
        episodeService.deleteAll();
    }

    @Test
    public void getIndex() throws Exception {
        Mockito.when(episodeService.findOne(episode.getId())).thenReturn(episode);

        mockMvc.perform(MockMvcRequestBuilders.get(MainController.HOME + EpisodeAPIController.API_EPISODE +
                MainController.HOME + episode.getId()))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(jsonPath("$.id").value(episode.getId()))
                .andExpect(jsonPath("$.name").value(episode.getName()))
                .andExpect(jsonPath("$.number").value(episode.getNumber()))
                .andExpect(jsonPath("$.productionId").value(episode.getProductionId()))
                .andDo(print());
    }

    public static byte[] convertObjectToJsonBytes(Object object) throws IOException {
        ObjectMapper mapper = new ObjectMapper();
        mapper.setSerializationInclusion(JsonInclude.Include.NON_NULL);
        return mapper.writeValueAsBytes(object);
    }
}
