package za.co.makeapp.episode;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import za.co.makeapp.TestConfig;
import za.co.makeapp.data.builder.EpisodeBuilder;
import za.co.makeapp.data.builder.EpisodeBuilder;
import za.co.makeapp.data.model.Episode;
import za.co.makeapp.data.model.Episode;
import za.co.makeapp.data.repository.EpisodeRepository;
import za.co.makeapp.data.repository.EpisodeRepository;
import za.co.makeapp.episode.service.EpisodeService;
import za.co.makeapp.episode.service.EpisodeService;

import java.util.Collection;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;

/**
 * Created by nguni52 on 11/12/16.
 */
public class EpisodeServiceTests  extends TestConfig {
    private Log log = LogFactory.getLog(this.getClass().getName());
    Episode episode;
    @Autowired
    EpisodeService episodeService;
    @Autowired
    private EpisodeRepository episodeRepository;

    @Before
    public void setUp() {
        episode = EpisodeBuilder.buildAnEpisode(1);
    }

    @After
    public void tearDown() {
        episodeRepository.deleteAll();
    }

    @Test
    public void testCreateEpisode() {
        log.info("RUNNING TEST CREATE EPISODE\n\n\n\n");
        episodeService.create(episode);
        Episode newEpisode = episodeService.findOne(episode.getId());
        assertNotNull(episode);
        assertNotNull(newEpisode);
        assertEquals(episode.getName(), newEpisode.getName());
        assertEquals(episode, newEpisode);
        episodeRepository.delete(newEpisode);
    }

    @Test
    public void testGetEpisodes() {
        log.info("RUNNING TEST GET EPISODES\n\n\n\n");
        List<Episode> episodes = EpisodeBuilder.buildEpisodes(10);
        assertNotNull(episodes);
        assertEquals(10, episodes.size());
        episodeRepository.save(episodes);
        List<Episode> tempEpisodes = episodeService.findAll("production1");
        assertNotNull(tempEpisodes);
        log.info("Episode 1: " + tempEpisodes.iterator().next().toString() + "\n\n\n");
        assertEquals(tempEpisodes.size(), episodes.size());

    }

    @Test
    public void testUpdateEpisode() {
        log.info("RUNNING TEST update episode\n\n\n\n");
        episode = EpisodeBuilder.buildAnEpisode(11);
        episodeService.create(episode);
        String name = "New Name";
        String number = "New number";
        String productionId = "production2";
        episode.setName(name);
        episode.setNumber(number);
        episode.setProductionId(productionId);

        episodeService.update(episode);

        Episode newEpisode = episodeService.findOne(episode.getId());
        assertEquals(name, newEpisode.getName());
        assertEquals(number, newEpisode.getNumber());
        assertEquals(productionId, newEpisode.getProductionId());
        assertEquals(episode, newEpisode);

        episodeRepository.delete(newEpisode);
    }

    @Test
    public void testDeleteEpisode() {
        episode = EpisodeBuilder.buildAnEpisode(12);
        episodeService.create(episode);
        episodeService.delete(episode.getId());
//        episodeRepository.delete(episode);
        Episode newEpisode = episodeService.findOne("12");
        assertNull(newEpisode);
    }

    @Test
    public void testDeleteAll() {
        List<Episode> episodes = EpisodeBuilder.buildEpisodes(10);
        assertNotNull(episodes);
        assertEquals(10, episodes.size());
        episodeService.deleteAll();
        episodes = episodeService.findAll("production1");
        assertEquals(0, episodes.size());
    }
}
