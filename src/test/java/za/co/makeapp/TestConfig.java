package za.co.makeapp;

import org.junit.runner.RunWith;
import org.springframework.boot.test.IntegrationTest;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

/**
 * Created by nguni52 on 2016/06/27.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = MakeAppManagerApplication.class)
//@ActiveProfiles(value = "test")
@IntegrationTest() //{"server.port=0"}
public class TestConfig {
}
